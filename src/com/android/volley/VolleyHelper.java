
package com.android.volley;

import android.content.Context;

import com.android.volley.toolbox.BitmapLruCache;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class VolleyHelper {

    protected ImageLoader mImageLoader;
    protected BitmapLruCache mBitmapLruCache;
    protected RequestQueue mRequestQueue;

    private static VolleyHelper sInstance;

    private VolleyHelper(Context context) {
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        mBitmapLruCache = new BitmapLruCache();
        mImageLoader = new ImageLoader(mRequestQueue, mBitmapLruCache);
    }

    public static final VolleyHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new VolleyHelper(context);
        }

        return sInstance;
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }
}
