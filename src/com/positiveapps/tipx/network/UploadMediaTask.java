/**
 * 
 */
package com.positiveapps.tipx.network;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.Header;
import org.apache.http.impl.io.HttpResponseWriter;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.Conversation;
import com.positiveapps.tipx.util.DialogUtil;
import com.positiveapps.tipx.util.ToastUtil;

/**
 * @author natiapplications
 * 
 */
public class UploadMediaTask {

	private final String TAG = "UploadMediaTask";
	private final String URL = NetworkManager.SUBMIT_MEDIA_FILE+"?UserID="+TipxApp.userProfil.getUserId();
	private AsyncHttpClient request;
	private File myFile;
	private int mediaType;
	private String fileName;
	private String urlResponse;
	private OnFileUploaded callBack;
	private int messageChatType;
	private String id;
	
	public UploadMediaTask (int mediaType,String fileName,int messageChatType,String id,OnFileUploaded callback){
		this.mediaType = mediaType;
		this.messageChatType = messageChatType;
		this.id = id;
		this.fileName = fileName;
		this.myFile = new File(fileName);
		this.callBack = callback;
		
	}
	
	
	public void uploudAsMultypart () {
		request = new AsyncHttpClient();
		request.setMaxRetriesAndTimeout(1, 100000);
		RequestParams params = new RequestParams();
		try {
		    params.put("file", myFile);
		} catch(FileNotFoundException e) {}
		
		request.post(URL, params, new JsonHttpResponseHandler(){
			
			
			
			@Override
			public void onSuccess(int statusCode, Header[] headers,JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				//DialogUtil.dismisDialog(dialog);
				boolean success = false;
				try {
					JSONObject data = response.optJSONObject("data");
					urlResponse = data.optString("url");
					TipxApp.conversationManager.removeMessageByContent(fileName);
					String mediaCode = ChatMessage.MEDIA_CODE_VIDEO;
					if (mediaType == ChatMessage.TYPE_AUDIO){
						mediaCode = ChatMessage.MEDIA_CODE_AUDIO;
					}
					if(messageChatType == ChatMessage.GROUP_MESSAGE){
						TipxApp.chatManager.sendMediaMessage(mediaCode,"0",id,
								urlResponse,fileName);
					}else{
						TipxApp.chatManager.sendMediaMessage(mediaCode,id,"0",
								urlResponse,fileName);
					}
					success = true;
				} catch (Exception e) {
					Log.e("exceptionLog", "ex = " + e.getMessage());
					ToastUtil.toster(TipxApp.appContext.getString(R.string.upload_failded_txt), true);
					success = false;
				}
				if (callBack != null){
					callBack.onUpload(success, urlResponse, fileName, mediaType);
				}
			}
			
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				Log.e("onFiloar", "onFiloar");
				ToastUtil.toster(TipxApp.appContext.getString(R.string.upload_failded_txt), true);
				TipxApp.conversationManager.updateMessageUploadStatusByContent(fileName);
				if (callBack != null){
					callBack.onUpload(false, urlResponse, fileName, mediaType);
				}
			}
			
			
			@Override
			public void onCancel() {
				super.onCancel();
				Log.e("onFiloar", "onFiloar");
				
				TipxApp.conversationManager.updateMessageUploadStatusByContent(fileName);
				if (callBack != null){
					callBack.onUpload(false, urlResponse, fileName, mediaType);
				}
			}
			
			
			
		});
		
	}
	
	
	public void uploadAsBase64(final String imagePath){
		
		TipxApp.networkManager.submitMedia(fileName, new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				if (!isHasError){
					boolean success = false;
					try {
						JSONObject data = new JSONObject(response.getData());
						urlResponse = data.optString("url");
						
						if(messageChatType == ChatMessage.GROUP_MESSAGE){
							TipxApp.chatManager.sendMediaMessage(ChatMessage.MEDIA_CODE_IMAGE,"0",id,
									urlResponse,imagePath);
						}else{
							TipxApp.chatManager.sendMediaMessage(ChatMessage.MEDIA_CODE_IMAGE,id,"0",
									urlResponse,imagePath);
						}
						success = true;
					} catch (Exception e) {
						ToastUtil.toster(TipxApp.appContext.getString(R.string.upload_failded_txt), true);
						success = false;
					}
					if (callBack != null){
						callBack.onUpload(success, urlResponse, fileName, mediaType);
					}
				}else{
					ToastUtil.toster(TipxApp.appContext.getString(R.string.upload_failded_txt), true);
					ToastUtil.toster(erroDescription, false);
				}
			}
			
		});
		
	}
	
	public interface OnFileUploaded {
		
		public void onUpload (boolean success,String dataURL,String fileName,int requestType);
	}
	

}
