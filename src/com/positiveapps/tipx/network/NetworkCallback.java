/**
 * 
 */
package com.positiveapps.tipx.network;

import org.json.JSONObject;

import com.positiveapps.tipx.util.ToastUtil;

import android.util.Log;




/**
 * @author Nati GAbay
 *
 */
public class NetworkCallback {
	
	/**
	 * called when Internet connection is not available
	 */
	public void networkUnavailable (String message){
		Log.e("RequestTasklog", "network was not anabled");
		ToastUtil.toster(message, false);
	};
	
	
	/**
	 * called when the response returned from the server as jsonObject. in this method we parsing the response by convert it to some object
	 * the object returned by this method will provide by the onDataReceive method.
	 * 
	 * @param toParse - the response as jsonObject
	 * @return the object created by the parsing response
	 */
	public ResponseObject parseResponse (JSONObject toParse){
		return new ResponseObject(toParse);
	}
	
	public ResponseObject parseResponse (String toParse){
		return new ResponseObject(toParse);
	}
	
	/**
	 * called when the response has been success. in this case the method provides the object which created from the response 
	 * and boolean that indicate if has some error including in the response with the error description
	 * 
	 * @param response - the object which created by the response
	 * @param isHasError - indicate if has some error including in the response
	 * @param erroDescription - error description ( null if there is not any errors)
	 */
	public void onDataRecived (ResponseObject response,boolean isHasError,String erroDescription){};
	
	/**
	 * called when the request failed 
	 */
	public void onError (){
		Log.e("RequestTasklog", "has error");
		//ToastUtil.toster("has error", false);
	};

}
