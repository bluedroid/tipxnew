package com.positiveapps.tipx;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.tipx.adapters.SizeAdapter;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.SoundUtil;
import com.skd.androidrecording.video.AdaptiveSurfaceView;
import com.skd.androidrecording.video.CameraHelper;
import com.skd.androidrecording.video.VideoRecordingHandler;
import com.skd.androidrecording.video.VideoRecordingManager;

public class RecordingVideoActivity extends FragmentActivity implements OnClickListener {
	
	public static final String EXTRA_FILE_NAME = "file_name";
	public static final String RESULT_DATA = "data_result";
	
	private final int TIME_LIMIT = 60;
	
    private  String fileName = null;
	private Button recordBtn;
	private ImageView saveBtn;
	private ImageButton switchBtn;
	private Spinner videoSizeSpinner;
	private TextView timerTxt;
	private Size videoSize = null;
	private int timerCounter;
	private VideoRecordingManager recordingManager;
	private boolean cancel;
	private boolean stop;
	private boolean startTimer;
	private ImageView imageAnimation;
	
	private VideoRecordingHandler recordingHandler = new VideoRecordingHandler() {
		@Override
		public boolean onPrepareRecording() {
			if (videoSizeSpinner == null) {
	    		initVideoSizeSpinner();
	    		return true;
			}
			return false;
		}
		
		@Override
		public Size getVideoSize() {
			return videoSize;
		}
		
		@Override
		public int getDisplayRotation() {
			return RecordingVideoActivity.this.getWindowManager().getDefaultDisplay().getRotation();
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeFullScreenActivity(this);
		setContentView(R.layout.activity_recording_video);
		MainActivity.appSentToBackground = false;
		fileName = getIntent().getExtras().getString(EXTRA_FILE_NAME);
		imageAnimation = (ImageView)findViewById(R.id.new_image);
		AdaptiveSurfaceView videoView = (AdaptiveSurfaceView) findViewById(R.id.videoView);
		timerTxt = (TextView)findViewById(R.id.timer_tex);
		timerTxt.setText("01:00");
		timerCounter = TIME_LIMIT;
		
		recordingManager = new VideoRecordingManager(videoView, recordingHandler);
		
		recordBtn = (Button) findViewById(R.id.recordBtn);
		switchBtn = (ImageButton) findViewById(R.id.switchBtn);
		saveBtn = (ImageView) findViewById(R.id.saveBtn);
		
		
		saveBtn.setOnClickListener(this);
		saveBtn.setVisibility(View.GONE);
		recordBtn.setOnClickListener(this);

		
		
	}
	
	@Override
	protected void onDestroy() {
		try {
			recordingManager.dispose();
			recordingHandler = null;
			cancel= true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onDestroy();
	}
	
	@SuppressLint("NewApi")
	private void initVideoSizeSpinner() {
		videoSizeSpinner = (Spinner) findViewById(R.id.videoSizeSpinner);
		if (Build.VERSION.SDK_INT >= 11) {
			List<Size> sizes = CameraHelper.getCameraSupportedVideoSizes(recordingManager.getCameraManager().getCamera());
			videoSizeSpinner.setAdapter(new SizeAdapter(sizes));
			videoSizeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					videoSize = (Size) arg0.getItemAtPosition(arg2);
					recordingManager.setPreviewSize(videoSize);
				}
	
				@Override
				public void onNothingSelected(AdapterView<?> arg0) {}
			});
			videoSize = (Size) videoSizeSpinner.getItemAtPosition(0);
		}
		else {
			videoSizeSpinner.setVisibility(View.GONE);
		}
	}
	
	
	
	private void record() {
		if (recordingManager.stopRecording()) {
			SoundUtil.playSound(R.raw.cancel_record_sound);
			recordBtn.setText(R.string.recordBtn);
			switchBtn.setEnabled(true);
			saveBtn.setEnabled(true);
			videoSizeSpinner.setEnabled(true);
			recordBtn.setSelected(false);
			saveBtn.setVisibility(View.VISIBLE);
			stop = true;
			//imageAnimation.clearAnimation();
		}
		else {
			recordBtn.setSelected(true);
			saveBtn.setVisibility(View.GONE);
			SoundUtil.playSound(R.raw.record_sound);
			stop = false;
			if (!startTimer){
				startTimer();
			}
			startRecording();
			//startAnimation();
		}
	}
	
	private void startAnimation (){
		imageAnimation.setBackgroundResource(R.drawable.close_btn_animation);
		 AnimationDrawable frameAnimation = (AnimationDrawable) imageAnimation.getBackground();
		 frameAnimation.setOneShot(true);
		 frameAnimation.start();
	}
	
	private void startRecording() {
		if (recordingManager.startRecording(fileName, videoSize)) {
			recordBtn.setText(R.string.stopRecordBtn);
			switchBtn.setEnabled(false);
			saveBtn.setEnabled(false);
			videoSizeSpinner.setEnabled(false);
			return;
		}
		Toast.makeText(this, getString(R.string.videoRecordingError), Toast.LENGTH_LONG).show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.saveBtn:
			stopRecordingAndResultActivity();
			break;
			
		case R.id.recordBtn:
			record();
			break;
		}
		
	}

	
	private void stopRecordingAndResultActivity() {
		try {
			recordingManager.dispose();
			recordingHandler = null;
		} catch (Exception e) {}
		
		Intent data = new Intent();
		Bundle extra = new Bundle();
		extra.putSerializable(RESULT_DATA, fileName);
		data.putExtras(extra);
		setResult(Activity.RESULT_OK,data);
		finish();
	}
	
	private void startTimer (){
		startTimer = true;
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while (!cancel){
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						// TODO: handle exception
					}
					if (!stop){
						timerCounter -= 1;
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								String counter = "";
								if (timerCounter < 10){
									counter = "0"+timerCounter;
								}else{
									counter = String.valueOf(timerCounter);
								}
								timerTxt.setText("00:" + counter );
							}
						});
					}
					if (timerCounter == 0){
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								try {
									SoundUtil.playSound(R.raw.cancel_record_sound);
									recordingManager.stopRecording();
								} catch (Exception e) {}
								cancel = true;
								recordBtn.setVisibility(View.GONE);
								saveBtn.setVisibility(View.VISIBLE);
								saveBtn.setEnabled(true);
							}
						});
						
					}
					
				}
			
			}
		}).start();
	}
	
	
}