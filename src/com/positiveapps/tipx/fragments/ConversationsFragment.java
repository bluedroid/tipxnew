/**
 * 
 */
package com.positiveapps.tipx.fragments;



import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.braunster.tutorialview.object.Tutorial;
import com.braunster.tutorialview.object.TutorialBuilder;
import com.braunster.tutorialview.object.TutorialIntentBuilder;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.adapters.AppContactsListAdapter;
import com.positiveapps.tipx.adapters.ConversationsListAdapter;
import com.positiveapps.tipx.chat.TipxChatListener;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.database.StorageListener;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.dialogs.LockPasswordDialog;
import com.positiveapps.tipx.fragments.login.ProfileFragment;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.Conversation;
import com.positiveapps.tipx.objects.GroupsList;
import com.positiveapps.tipx.objects.UserProfile;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.DialogUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.ToastUtil;




/**
 * @author natiapplications
 *
 */
public class ConversationsFragment extends BaseFragment implements OnItemClickListener,TipxChatListener{
	
	

	private View listHeader;
	private TextView headerText;
	private LinearLayout headerContainer;
	private ListView convarsationListView;
	private ConversationsListAdapter adapter;
	
	private ImageView contactBtn;
	private ImageView addConversationBtn;
	private ImageView settingsBtn;
	
	public ConversationsFragment() {

	}

	public static ConversationsFragment newInstance() {
		ConversationsFragment instance = new ConversationsFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_conversations, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.setScreenTitle(TipxApp.appContext.getString(R.string.converstions_screen_title),null,null);
		
		
		convarsationListView = (ListView)view.findViewById(R.id.conversations_list);
		listHeader = getActivity().getLayoutInflater().inflate(R.layout.layout_app_contacts_list_header, null);
		headerText = (TextView)listHeader.findViewById(R.id.header_text);
		headerContainer = (LinearLayout)view.findViewById(R.id.header_place);
		
		convarsationListView.setOnItemClickListener(this);
		headerText.setText("");
		
		setUpActionBarButtons();
		setUpConversions();
		
	}
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MainActivity.setConversationListener(this);
		getOnlineContacts(TipxApp.contactsManager.getAppContactsList());
	}
	
	int tempCounter;
	
	private void setUpActionBarButtons (){
		MainActivity.removeAllActionBarButtons();
		MainActivity.hideProgressBar();
		ActionBarButton contactBtn = new ActionBarButton(getActivity(), R.drawable.ic_action_group, "",
				new OnActionBarButtonClickListener() {
					@Override
					public void onActionBarButtonClick(View v) {
						getFragmentManager().popBackStack();
					}
				});
		this.contactBtn = contactBtn.getImageView();
		MainActivity.addButtonToActionBar(contactBtn);
		ActionBarButton addBtn = new ActionBarButton(getActivity(), R.drawable.ic_action_new, "",
				new OnActionBarButtonClickListener() {
					@Override
					public void onActionBarButtonClick(View v) {
						showAddConversationDialog();
					}
				});
		this.addConversationBtn = addBtn.getImageView();
		MainActivity.addButtonToActionBar(addBtn);
		ActionBarButton settingsBtn = new ActionBarButton(getActivity(), R.drawable.ic_action_settings, "",
				new OnActionBarButtonClickListener() {
					@Override
					public void onActionBarButtonClick(View v) {
						FragmentsUtil.openFragmentDownToUp(getFragmentManager(),
								ProfileFragment.newInstance(true), R.id.main_container);
					}
				});
		this.settingsBtn = settingsBtn.getImageView();
		MainActivity.addButtonToActionBar(settingsBtn);
		
	}
	
	
	@Override
	public void startViewAnimations(View view) {
		// TODO Auto-generated method stub
		super.startViewAnimations(view);
		showActionsExplanationIfNeeded();
	}
	
	
	/**
	 * 
	 */
	private void showActionsExplanationIfNeeded() {
		if (!TipxApp.generalSettings.isCahtsSCreenEXplanationShown()){
			TutorialIntentBuilder builder = new TutorialIntentBuilder(getActivity());
			TutorialBuilder tBuilder = new TutorialBuilder();
			tBuilder.setBackgroundColor(getResources().getColor(R.color.blue_trans));
			
			ArrayList<Tutorial> tutorials = new ArrayList<Tutorial>();
			tutorials.add(AppUtil.createTutorial(contactBtn,
					getString(R.string.explanation_chats_contact)));
			tutorials.add(AppUtil.createTutorial(addConversationBtn,
					getString(R.string.explanation_chats_add_chat)));
			tutorials.add(AppUtil.createTutorial(settingsBtn,
					getString(R.string.explanation_chats_settings)));
			
			builder.skipTutorialOnBackPressed(true);
			builder.setWalkThroughList(tutorials);
			TipxApp.generalSettings.setChatScreenExplanationShow(true);
			startActivity(builder.getIntent());
			getActivity().overridePendingTransition(R.anim.dummy, R.anim.dummy);
		}
	}
	
	protected void setUpConversions() {
		TipxApp.conversationManager.loadConversations(new StorageListener() {
			@Override
			public void onDataStorageRcived(String type, Object object) {
				
				updateConversationListView((ArrayList<Conversation>)object); 
				Log.e("loadConversation", "conversation size - " + ((ArrayList<Conversation>)object).size());
				getMyGroups();
			}
			@Override
			public void DataNotFound(String type) {}
		});
	}

	public void updateConversationListView(ArrayList<Conversation> conversation) {

		if (conversation.size() == 0) {
			headerContainer.removeAllViews();
			headerContainer.addView(listHeader);
			headerText.setText("No result found");
		} else {
			try {
				headerContainer.removeAllViews();
			} catch (Exception e) {
			}
		}

		adapter = new ConversationsListAdapter(getActivity(), TipxApp.conversationManager.getConversionsAsList());
		convarsationListView.setAdapter(adapter);

	}

	private void showAddConversationDialog() {
		String [] optionsText = getActivity().getResources().getStringArray(R.array.add_conversation_dialog);
		DialogManager.showDialogChooser(this,optionsText, new DialogCallback(){
			
			@Override
			protected void onDialogOptionPressed(int which,
					DialogFragment dialog) {
				super.onDialogOptionPressed(which, dialog);
				switch (which) {
				case 0:
					getFragmentManager().popBackStack();
					break;
				case 1:
					FragmentsUtil.openFragmentDownToUp(getFragmentManager(), 
							CreateGroupFragment.newInstance(), R.id.main_container);
					break;
				}
			}
			
		});
		
	}
	
	
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Conversation conversation = TipxApp.conversationManager.getConversionsAsList().get(position);
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
				  ChatContainerFragment.newInstance(conversation), R.id.main_container);
		
	}
	
	private void getMyGroups (){
		MainActivity.showProgressBar();
		TipxApp.networkManager.getMemberGroups(new NetworkCallback(){

			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				GroupsList responseObject = new GroupsList(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				MainActivity.hideProgressBar();
				Log.i("createuserlog", "response = " + response.getJsonContent());
				if (!isHasError){
					GroupsList result = (GroupsList)response;
					Log.i("createuserlog", "size = " + result.getGroupsArray().size());
					try {
						for (int i = 0; i < result.getGroupsArray().size(); i++) {
							TipxApp.conversationManager.openGroupConversation(result.getGroupsArray().get(i)
									, new ArrayList<ChatMessage>());
						}
						adapter.notifyDataSetChanged();
					} catch (Exception e) {
						e.printStackTrace();
						Log.e("createExlog", "ex = " +e.getMessage());
					}
				}else{
					ToastUtil.toster(erroDescription, false);
				}
				
			}
		
			@Override
			public void onError() {
				super.onError();
				MainActivity.hideProgressBar();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				MainActivity.hideProgressBar();
			}
		});
	}

	
	@Override
	public void onChatMessageReceived(ChatMessage msg) {
		Log.e("currentloglost", "type -= " + msg.getType());
		if (msg.getType() == ChatMessage.TYPE_ME_JOIN||msg.getType() == ChatMessage.TYPE_ME_LEAVE||
				msg.getType() == ChatMessage.TYPE_GROUP_DELETED){
			Log.d("currentloglost", "type -= " + msg.getType());
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					getMyGroups();
				}
			});
		}else if (msg.getType() == ChatMessage.TYPE_DELET_CONVERSATION||msg.getType() == ChatMessage.TYPE_DELET_CONTACT||
				msg.getType() == ChatMessage.TYPE_MESSAGE_DELETED||
				msg.getType() ==11||msg.getType() ==12||msg.getType() ==13||
				msg.getType() ==1||msg.getType() ==2||msg.getType() ==3){
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					updateConversationListView(TipxApp.conversationManager.getConversionsAsList());
				}
			});
		}
	}

	private void getOnlineContacts (ArrayList<AppContact> toSet){
		AppContact.getOnlineContacts(toSet,new NetworkCallback(){

			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				MainActivity.hideProgressBar();

				if (!isHasError){
					try {
						JSONObject data = new JSONObject(response.getData());
						JSONArray contactsResult = data.optJSONArray("ApplicationStatusFriends");
						TipxApp.contactsManager.updateContactsPresentStatus(contactsResult);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					ToastUtil.toster(erroDescription, false);
				}
				
			}
		});
	}
	

}
