/**
 * 
 */
package com.positiveapps.tipx.fragments;



import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.BackStackUtil;
import com.positiveapps.tipx.util.DialogUtil;




/**
 * @author Nati Gabay
 *
 */
public class BaseFragment extends Fragment implements OnGlobalLayoutListener{
	
	protected ProgressDialog progressDialog;
	public boolean fragIsOn;
	protected String screenName; 
	
	public boolean onBackPressed () {
		return false;
	}
	
	public void showProgressDialog(String message) {
		this.progressDialog = 
				DialogUtil.showProgressDialog(getActivity(), message);
	}
	
	public void showProgressDialog() {
		try {
			this.progressDialog = 
			         DialogUtil.showProgressDialog(getActivity(), 
			        		 getString(R.string.deafult_dialog_messgae));
		} catch (Exception e) {}
	}
	
	public void dismisProgressDialog() {
		DialogUtil.dismisDialog(this.progressDialog);
	}
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		fragIsOn = true;
		MainActivity.currentFragment = this;
		view.getViewTreeObserver().addOnGlobalLayoutListener(this);
		BackStackUtil.addToBackStack(getClass().getSimpleName());
		AppUtil.setTextFonts(getActivity(),view);
		
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		fragIsOn = false;
		BackStackUtil.removeFromBackStack(getClass().getSimpleName());
		AppUtil.hideKeyBoard(getActivity());
		
	}
	
	@SuppressLint("NewApi")
	@Override
	public void onGlobalLayout() {
		try {
			getView().getViewTreeObserver().removeOnGlobalLayoutListener(this); 
			
			startViewAnimations(getView());
		} catch (Exception e) {} catch (Error e){}
		
	}
	
	public void startViewAnimations(View view){}

}
