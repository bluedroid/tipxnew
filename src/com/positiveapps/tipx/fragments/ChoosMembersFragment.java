/**
 * 
 */
package com.positiveapps.tipx.fragments;



import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.positiveapps.tipx.PostActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.adapters.AppContactsListAdapter;
import com.positiveapps.tipx.adapters.AppContactsListAdapter.OnContactSelectedListener;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.database.StorageListener;
import com.positiveapps.tipx.objects.GroupObject;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;





/**
 * @author natiapplications
 *
 */
public class ChoosMembersFragment extends BaseFragment implements TextWatcher,OnItemClickListener,OnContactSelectedListener{
	

	public static final String 	EXTRA_GROUP_OBJECT = "ExtraGroupObject";
	public static final String 	RESULT_DATA = "ResultData";
	
	private View listHeader;
	private TextView headerText;
	private EditText searchEt;
	private LinearLayout headerContainer;
	private ListView contactsListView;
	private AppContactsListAdapter adapter;
	
	private boolean isOnSearchResult;
	private ArrayList<AppContact> filterArray = new ArrayList<AppContact>();
	private ArrayList<AppContact> searchArray;
	private ArrayList<AppContact> currentArray;
	private ArrayList<AppContact> selectedArray = new ArrayList<AppContact>();
	
	private GroupObject groupObject;
	
	public ChoosMembersFragment() {

	}

	public static ChoosMembersFragment newInstance(GroupObject groupObject) {
		ChoosMembersFragment instance = new ChoosMembersFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_GROUP_OBJECT, groupObject);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_app_contacts, container,
				false);
		groupObject = (GroupObject)getArguments().getSerializable(EXTRA_GROUP_OBJECT);
		return rootView;
	}

	@SuppressLint("InflateParams")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		PostActivity.setScreenTitle(TipxApp.appContext.getString(R.string.choose_members_screen_title));
		
		
		contactsListView = (ListView)view.findViewById(R.id.contacts_list);
		listHeader = getActivity().getLayoutInflater().inflate(R.layout.layout_app_contacts_list_header, null);
		headerText = (TextView)listHeader.findViewById(R.id.header_text);
		headerText.setText("No contacts to add found");
		searchEt = (EditText)view.findViewById(R.id.search_et);
		headerContainer = (LinearLayout)view.findViewById(R.id.header_container);
		getActivity().getWindow().setSoftInputMode(
			    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
			);
		searchEt.addTextChangedListener(this);
		
		contactsListView.setOnItemClickListener(this);
		contactsListView.addHeaderView(listHeader);
		setUpActionBarButtons();
		setUpContacts();
	}
	
	
	
	
	
	private void setUpActionBarButtons (){
		PostActivity.removeAllActionBarButtons();
		ActionBarButton saveBtn = new ActionBarButton(getActivity(), R.drawable.empty, getString(R.string.save),
				new OnActionBarButtonClickListener() {
					@Override
					public void onActionBarButtonClick(View v) {
						Intent data = new Intent();
						Bundle extra = new Bundle();
						
						extra.putSerializable(RESULT_DATA, selectedArray);
						data.putExtras(extra);
						
						if (selectedArray.size() <= 0){
							getActivity().setResult(Activity.RESULT_CANCELED);
						}else{
							getActivity().setResult(Activity.RESULT_OK,data);
						}
						getActivity().finish();
						
					}
				});
		PostActivity.addButtonToActionBar(saveBtn);
		
	}
	
	
	protected void setUpContacts() {
		TipxApp.contactsManager.readContactsList(new StorageListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void onDataStorageRcived(String type, Object object) {
				filterArray = new ArrayList<AppContact>();
				for (int i = 0; i < ((ArrayList<AppContact>)object).size(); i++) {
					AppContact temp = ((ArrayList<AppContact>)object).get(i);
					if (temp.isHasApplication()){
						if (!isMemberAllradyAdded(temp)){
							filterArray.add(temp);
						}
					}
				}
				Log.e("creategrouplog", "size = " + filterArray.size());
				updateContactListView(filterArray);
			}
			@Override
			public void DataNotFound(String type) {}
		});
	}

	
	public boolean isMemberAllradyAdded (AppContact appContact){
		if (groupObject == null){
			if (TipxApp.userProfil.getUserId().equals(appContact.getContactId())){
				return true;
			}
			return appContact.isHidenContact();
		}
		for (int i = 0; i < groupObject.getMembers().size(); i++) {
			if (appContact.getContactId().equals(groupObject.getMembers().get(i).getId()+"")){
				return true;
			}
		}
		return false;
	}
	
	
	public void updateContactListView (ArrayList<AppContact> contacts){
		currentArray = contacts;
		
		if (isOnSearchResult){
			if(contacts.size() == 0){
				headerContainer.removeAllViews();
				headerContainer.addView(listHeader);
				headerText.setText("No result found");
			}else{
				try {
					headerContainer.removeAllViews();
				} catch (Exception e) {}
			}
		}else{
			if (currentArray.size() > 0){
				//setAppContactStatus(TipxApp.contactsManager.getAppContactsList());
				try {
					contactsListView.removeHeaderView(listHeader);
				} catch (Exception e) {}
			}else{
				searchEt.setVisibility(View.GONE);
			}
		}
		
		adapter = new AppContactsListAdapter(getActivity(), currentArray);
		adapter.setListAsChooser("",this);
		contactsListView.setAdapter(adapter);
		
	}

	public String getSelectedContactAsString (){
		
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (int i = 0; i < selectedArray.size(); i++) {
			
			builder.append(selectedArray.get(i).getContactId());
			
			if (i < selectedArray.size()-1){
				builder.append(",");
			}
		}
		builder.append("]");
		return builder.toString();
	}
	

	
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {}

	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {}

	
	@Override
	public void afterTextChanged(Editable s) {
		if (searchEt.getText().toString().isEmpty()){
			updateContactListView(filterArray);
			isOnSearchResult = false;
		}else{
			isOnSearchResult = true;
			search(searchEt.getText().toString());
		}
		
	}

	public void search (String keyWord){
		searchArray = new ArrayList<AppContact>();
		for (int i = 0; i < filterArray.size(); i++) {
			AppContact temp = filterArray.get(i);
			if(StringUtils.containsIgnoreCase(temp.getContactName(), keyWord)||
					StringUtils.containsIgnoreCase(temp.getContactPhone(), keyWord)){
				searchArray.add(temp);
			}
		}
		updateContactListView(searchArray);
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		AppContact selected = currentArray.get(position);
		CheckBox checkBox = (CheckBox)view.findViewById(R.id.checkContact);
		checkBox.toggle();
		onContactSelected(checkBox.isChecked(), selected);
		
	}

	
	
	@Override
	public boolean onBackPressed() {
		if(isOnSearchResult){
			isOnSearchResult = false;
			searchEt.setText("");
			return true;
		}else{
			return false;
		}
	}

	
	@Override
	public void onContactSelected(boolean selected, AppContact contact) {
		Log.e("selectedcontactLog", "selected ? " + selected + " cotact = " + contact.getContactId());
		if (selected){
			selectedArray.add(contact);
		}else{
			try {
				selectedArray.remove(contact);
			} catch (Exception e) {}
		}
	}

}
