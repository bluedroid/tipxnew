/**
 * 
 */
package com.positiveapps.tipx.fragments;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONObject;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.positiveapps.tipx.LoginActivity;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.dialogs.LockPasswordDialog;
import com.positiveapps.tipx.fragments.BaseFragment;
import com.positiveapps.tipx.fragments.login.SetLockPasswordFragment;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.BitmapUtil;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.DeviceUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.ToastUtil;





/**
 * @author natiapplications
 *
 */
public class ContactProfileFragment extends BaseFragment implements OnClickListener {
	
	
	public static final String EXTRA_CONTACT = "Contact";
	private final int REQUEST_GALLERY = 1000;
	private final int REQUEST_GALLERY_FOR_BG = 2000;
	
	private FrameLayout sendMessageItem;
	private FrameLayout deletConversationItem;
	private FrameLayout deleteContactItem;
	private ImageView contactPicture;
	private EditText contactNameTv;
	private TextView connectionStatusTxt;
	
	//private ArrayList<FrameLayout> colorsFrames = new ArrayList<FrameLayout>();
	
	private Button editBgBtn;
	private ImageView previewBg;
	
	private TextView editTv;
	
	
	private String imageBase64;
	private AppContact currentContact;
	
	public ContactProfileFragment() {

	}

	public static ContactProfileFragment newInstance(AppContact contact) {
		ContactProfileFragment instance = new ContactProfileFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_CONTACT, contact);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contact_profile, container,
				false);
		currentContact = (AppContact)getArguments().getSerializable(EXTRA_CONTACT);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.currentFragment = this;
		MainActivity.setScreenTitle(currentContact.getContactName(),null,null);
		
		
		setUpActionBarButtons();
		
		sendMessageItem = (FrameLayout)view.findViewById(R.id.send_message);
		deletConversationItem = (FrameLayout)view.findViewById(R.id.delete_conversation);
		deleteContactItem = (FrameLayout)view.findViewById(R.id.delete_contact);
		
		contactPicture = (ImageView)view.findViewById(R.id.image_profile);
		connectionStatusTxt = (TextView)view.findViewById(R.id.connection_status_txt);
		
		contactNameTv = (EditText)view.findViewById(R.id.name_et);
		editTv = (TextView)view.findViewById(R.id.edit_txt);
		editBgBtn = (Button)view.findViewById(R.id.edit_Bg_btn);
		previewBg = (ImageView)view.findViewById(R.id.preview_bg);
		
		editBgBtn.setOnClickListener(this);
		
		
		/*colorsFrames.add((FrameLayout)view.findViewById(R.id.gray_color));
		colorsFrames.add((FrameLayout)view.findViewById(R.id.pink_color));
		colorsFrames.add((FrameLayout)view.findViewById(R.id.paper_color));
		colorsFrames.add((FrameLayout)view.findViewById(R.id.blue_color));
		
		for (int i = 0; i < colorsFrames.size(); i++) {
			colorsFrames.get(i).setOnClickListener(this);
		}*/
		
		
		sendMessageItem.setOnClickListener(this);
		deletConversationItem.setOnClickListener(this);
		deleteContactItem.setOnClickListener(this);
		contactPicture.setOnClickListener(this);
		editTv.setOnClickListener(this);
		
		fillScreenData();
		
		if (TipxApp.isFaceMood||!currentContact.isHasApplication()){
			sendMessageItem.setVisibility(View.GONE);
			deletConversationItem.setVisibility(View.GONE);
		}
		
	}

	private void setUpActionBarButtons() {

		MainActivity.removeAllActionBarButtons();
		ActionBarButton saveBtn = new ActionBarButton(getActivity(),
				R.drawable.empty, TipxApp.appContext.getString(R.string.save),
				new OnActionBarButtonClickListener() {

					@Override
					public void onActionBarButtonClick(View v) {
						currentContact.setContactName(contactNameTv.getText().toString());
						TipxApp.contactsManager.updateContactByContactPhone
						(currentContact.getContactPhone(), currentContact);
						TipxApp.conversationManager.updateConversationProperteisByContact
						(currentContact);
						getFragmentManager().popBackStack();
					}
				});
		MainActivity.addButtonToActionBar(saveBtn);

	}
	
	
	
	/**
	 * 
	 */
	private void fillScreenData() {
		
		String picturePath = currentContact.getContactImagePath();
		if (picturePath != null && !picturePath.isEmpty()){
			BitmapUtil.loadImageIntoByStringPath(BitmapUtil.IMAGE_TYPE_FROM_CAMERA,
					picturePath, contactPicture, 160, 2, R.drawable.ic_action_person);
		}else{
			picturePath = currentContact.getContactImgaeUrl();
			BitmapUtil.loadImageIntoByUrl(picturePath, contactPicture,
					R.drawable.ic_action_person, R.drawable.ic_action_person,
					150, 150, null);
		}
		contactNameTv.setText(currentContact.getContactName());
		//colorsFrames.get(currentContact.getContactColor()).setSelected(true);
		if (currentContact.isBackgroundIsImage()){
			BitmapUtil.loadImageIntoByFile(new File(currentContact.getBackgroundPath()), 
					previewBg, R.drawable.empty, R.drawable.empty, 200, 200, null);
		}else{
			previewBg.setImageResource(R.drawable.empty);
			previewBg.setBackgroundColor(currentContact.getContactColor());
		}
		setStatusHeader();
	}

	
	public void setStatusHeader () {
		String status = connectionStatusTxt.getText().toString();
    	
		int stat = currentContact.getContactStatus();
		if (stat == AppContact.STATUS_OFFLINE) {
			status = currentContact.getLastSeen();
		} else {
			status = status + " " +  AppContact.STATUS_DISPLAY[currentContact
					.getContactStatus()];
		}
		connectionStatusTxt.setText(status);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == REQUEST_GALLERY){
			if(resultCode == Activity.RESULT_OK){
				String path = BitmapUtil.getPathOfImagUri(data.getData());
				currentContact.setContactImagePath(path);
				imageBase64 = BitmapUtil.getImageAsStringEncodedBase64(
						BitmapUtil.IMAGE_TYPE_FROM_GALLERAY,data.getData(),BitmapFactory.decodeFile(path));
				BitmapUtil.loadImgaeIntoBySelectedUri(BitmapUtil.IMAGE_TYPE_FROM_GALLERAY,
						data.getData(), contactPicture, 160, 2, R.drawable.ic_action_person);
			}
		}
		if (requestCode == REQUEST_GALLERY_FOR_BG){
			if(resultCode == Activity.RESULT_OK){
				String path = BitmapUtil.getPathOfImagUri(data.getData());
				currentContact.setBackgroundIsImage(true);
				currentContact.setBackgroundPath(path);
				BitmapUtil.loadImageIntoByFile(new File(currentContact.getBackgroundPath()), 
						previewBg, R.drawable.empty, R.drawable.empty, 200, 200, null);
			}
		}
		
	}

	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}


	/*private void onColorSelected (int viewID) {
		for (int i = 0; i < colorsFrames.size(); i++) {
			if (viewID == colorsFrames.get(i).getId()){
				colorsFrames.get(i).setSelected(true);
				currentContact.setContactColor(i);
			}else{
				colorsFrames.get(i).setSelected(false);
			}
		}
	}*/
	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.blue_color:
		case R.id.pink_color:
		case R.id.gray_color:
		case R.id.paper_color:
			//onColorSelected(v.getId());
			break;
		
		case R.id.edit_Bg_btn:
			showChooseOptionDialog();
			break;
		case R.id.delete_contact:
			TipxApp.contactsManager.removeContactByContactPhone(currentContact.getContactPhone());
			TipxApp.conversationManager.removeConversation(currentContact.getContactId());
			if (currentContact.isHasApplication()){
				TipxApp.networkManager.deleteContact(currentContact.getContactId());
			}
			ToastUtil.toster(currentContact.getContactName() + " has been removed", false);
			getFragmentManager().popBackStack();
			if (ChatContainerFragment.containerInstance != null){
				ChatContainerFragment.closeFragmentAfterPrformRemoving = true;
			}
			break;
		case R.id.delete_conversation:
			TipxApp.conversationManager.removeConversation(currentContact.getContactId());
			TipxApp.networkManager.deleteChat(currentContact.getContactId());
			ToastUtil.toster("conversation with " + currentContact.getContactName() + " has been removed", false);
			getFragmentManager().popBackStack();
			if (ChatContainerFragment.containerInstance != null){
				ChatContainerFragment.closeFragmentAfterPrformRemoving = true;
			}
			break;
		case R.id.send_message:
			if (ChatContainerFragment.containerInstance != null){
				getFragmentManager().popBackStack();
				return;
			}
			final ChatContainerFragment frag = ChatContainerFragment.newInstance
			(TipxApp.conversationManager.openConversationWithContact(currentContact,
					new ArrayList<ChatMessage>()));
			
			FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
					frag, R.id.main_container);
			break;
		case R.id.image_profile:
			String picturePath = "";
			if (currentContact.getContactImagePath() != null && !currentContact.getContactImagePath().isEmpty()){
				picturePath =  currentContact.getContactImagePath();
			}else{
				picturePath = currentContact.getContactImgaeUrl();
			}
			if (picturePath != null&&!picturePath.isEmpty()){
				ContentProviderUtil.openImageInDevice(ContactProfileFragment.this,"image/*", picturePath);
			}else{
				ContentProviderUtil.openGallery(this, REQUEST_GALLERY);
			}
			break;
		case R.id.edit_txt:
			ContentProviderUtil.openGallery(this, REQUEST_GALLERY);
			break;
		}
		
	}

	
	/**
	 * 
	 */
	private void showChooseOptionDialog() {
		String [] optionsText = getActivity().getResources().getStringArray(R.array.set_background_dialog);
		DialogManager.showDialogChooser(this,optionsText, new DialogCallback(){
			
			@Override
			protected void onDialogOptionPressed(int which,
					DialogFragment dialog) {
				super.onDialogOptionPressed(which, dialog);
				switch (which) {
				case 0:
					showPickColorDialog(currentContact.getContactColor());
					break;
				case 1:
					ContentProviderUtil.openGallery(ContactProfileFragment.this, REQUEST_GALLERY_FOR_BG);
					break;
				}
			}
			
		});
		
	}

	private void showPickColorDialog(int previousColor) {
		DialogManager.showColorPickerDialog(previousColor,getFragmentManager(), new DialogCallback(){
			
			@Override
			protected void onDialogButtonPressed(int buttonID,
					DialogFragment dialog, Object Extra) {
				super.onDialogButtonPressed(buttonID, dialog, Extra);
				Integer color = (Integer)Extra;
				Log.e("colorpickerdialog", "color - " + color.toString());
				previewBg.setBackgroundColor(color);
				previewBg.setImageResource(R.drawable.empty);
				currentContact.setContactColor(color);
				currentContact.setBackgroundIsImage(false);
			}
		});
		
	}
	



}
