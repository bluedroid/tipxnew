/**
 * 
 */
package com.positiveapps.tipx.fragments.login;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONObject;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.positiveapps.tipx.LoginActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.adapters.CountryListAdapter;
import com.positiveapps.tipx.dialogs.AppDialogButton;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.CountryObject;
import com.positiveapps.tipx.objects.UserProfile;
import com.positiveapps.tipx.util.DialogUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.TextUtil;
import com.positiveapps.tipx.util.ToastUtil;




/**
 * @author natiapplications
 *
 */
public class RegistrationFragment extends Fragment implements OnClickListener,OnItemClickListener {
	
	private boolean fragIsOn;
	
	private final int MOOD_SEND_PHONE = 0;
	private final int MOOD_SEND_CODE = 1;
	
	private String[] countryNames;
	private String[] countryCods;
	private TypedArray countryFlags;
	
	private LinearLayout btnContainer;
	private LinearLayout chooseCountryItem;
	private ImageView countryFlagIv;
	private ImageView arrowIv;
	private TextView countryNameTv;
	private TextView explationTv;
	private EditText phoneEt;
	private Button sendBtn;
	private Button changeNumberBtn;
	private ProgressBar waitProgress;
	private TextView receivedCodeTv;
	private ListView countryList;
	private ArrayList<CountryObject> countreis = new ArrayList<CountryObject>();
	private CountryListAdapter adapter;
	
	private int currentMood;
	private CountryObject selectedCountry;
	
	public RegistrationFragment() {

	}

	public static RegistrationFragment newInstance() {
		RegistrationFragment instance = new RegistrationFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_registeration, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		LoginActivity.currentFragment = this;
		LoginActivity.setScreenTitle(TipxApp.appContext.getString(R.string.registration_screen_title));
		LoginActivity.removeAllActionBarButtons();
		fragIsOn = true;
		
		btnContainer = (LinearLayout)view.findViewById(R.id.btn_container);
		chooseCountryItem = (LinearLayout)view.findViewById(R.id.choose_country_item);
		countryFlagIv = (ImageView)view.findViewById(R.id.cuntry_flag);
		arrowIv = (ImageView)view.findViewById(R.id.arrow);
		countryNameTv = (TextView)view.findViewById(R.id.country_tv);
		explationTv = (TextView)view.findViewById(R.id.explanation_txt);
		phoneEt = (EditText)view.findViewById(R.id.phone_et);
		sendBtn = (Button)view.findViewById(R.id.send_btn);
		changeNumberBtn = (Button)view.findViewById(R.id.change_number_btn);
		countryList = (ListView)view.findViewById(R.id.country_list_view);
		waitProgress = (ProgressBar)view.findViewById(R.id.whait_progress);
		receivedCodeTv = (TextView)view.findViewById(R.id.received_code_tv);
		receivedCodeTv.setVisibility(View.GONE);
		waitProgress.setVisibility(View.GONE);
		countryList.setVisibility(View.GONE);
		arrowIv.setImageResource(R.drawable.ic_action_expand);
		
		countryList.setOnItemClickListener(this);
		chooseCountryItem.setOnClickListener(this);
		sendBtn.setOnClickListener(this);
		changeNumberBtn.setOnClickListener(this);
		
		setUpCountreisListView ();
		currentMood = MOOD_SEND_PHONE;
		
		
	}

	
	@Override
	public void onDestroy() {
		super.onDestroy();
		fragIsOn = false;
	}
	
	
	private void setUpCountreisListView() {
		countryNames = getResources().getStringArray(R.array.countries_name);
		countryCods = getResources().getStringArray(R.array.countries_calling_code);
		countryFlags = getResources().obtainTypedArray(R.array.countries_flags);
		
		for (int i = 0; i < countryNames.length; i++) {
			countreis.add(new CountryObject(countryNames[i],
					countryCods[i], countryFlags.getDrawable(i)));
		}
		
		adapter = new CountryListAdapter(getActivity(), countreis);
		countryList.setAdapter(adapter);
	}


	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		selectedCountry = countreis.get(arg2);
		countryFlagIv.setImageDrawable(selectedCountry.getCountryFlag());
		countryNameTv.setText(selectedCountry.getCountryName());
		chooseCountryItem.performClick();
		btnContainer.setVisibility(View.VISIBLE);
	}


	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.choose_country_item:
			if (countryList.getVisibility() == View.VISIBLE){
				countryList.setVisibility(View.GONE);
				arrowIv.setImageResource(R.drawable.ic_action_expand);
			}else{
				countryList.setVisibility(View.VISIBLE);
				arrowIv.setImageResource(R.drawable.ic_action_collapse);
			}
			break;
		case R.id.send_btn:
			
			if (currentMood == MOOD_SEND_PHONE){
				sendPhoneNumberToGetVertification();
			}else{
				sendConfirmationCode();
			}
			
			break;
		case R.id.change_number_btn:
			
			if (currentMood == MOOD_SEND_PHONE){
				return;
			}else{
				updateUI(true);
				currentMood = MOOD_SEND_PHONE;
			}
			
			break;
		}
		
	}
	
	@SuppressLint("NewApi")
	public void sendPhoneNumberToGetVertification (){
		if (selectedCountry == null){
			ToastUtil.toster(getString(R.string.empty_country), false);
			return;
		}
		
		if (TextUtil.baseFieldsValidation(phoneEt)){
			String numberString = selectedCountry.getCountryCode() + 
					phoneEt.getText().toString().substring(1,phoneEt.getText().toString().length());
			
			final ProgressDialog dialog = DialogUtil.showProgressDialog(getActivity(), getString(R.string.deafult_dialog_messgae));
			TipxApp.networkManager.createUser(numberString, new NetworkCallback(){
				
				@Override
				public ResponseObject parseResponse(JSONObject toParse) {
					ResponseObject responseObject = new ResponseObject(toParse);
					return responseObject;
				}
				
				@Override
				public void onDataRecived(ResponseObject response,
						boolean isHasError, String erroDescription) {
					super.onDataRecived(response, isHasError, erroDescription);
					DialogUtil.dismisDialog(dialog);
					Log.i("createuserlog", "response = " + response.getJsonContent());
					if (!isHasError){
						try {
							JSONObject data = new JSONObject(response.getData());
							String userId = data.optString("AppuserID");
							TipxApp.userProfil.setUserId(userId);
						} catch (Exception e) {
							// TODO: handle exception
						}
						currentMood = MOOD_SEND_CODE;
						updateUI(false);
						showWillGetConfirmationCodeDialog();
					}else{
						if(response.getError() == 5){
							resendVerificationCode(erroDescription);
						}else{
							ToastUtil.toster(erroDescription, false);
						}
						
					}
				}
			
				@Override
				public void onError() {
					super.onError();
					DialogUtil.dismisDialog(dialog);
					ToastUtil.toster(getString(R.string.general_network_error), false);
				}
				
				@Override
				public void networkUnavailable(String message) {
					super.networkUnavailable(message);
					DialogUtil.dismisDialog(dialog);
				}
			});
			
		}else{
			ToastUtil.toster(getString(R.string.empty_phone), false);
		}
		
	}
	
	/**
	 * 
	 */
	private void showWillGetConfirmationCodeDialog() {
		
		final int CONFIRM = 1;
		
		AppDialogButton[]  buttons = {new AppDialogButton
				(CONFIRM, R.drawable.blue_dialog_btn_selector, getString(R.string.confirm))}; 
		
		DialogManager.showAppDialog(getFragmentManager(), getString(R.string.thank_you), 
				getString(R.string.will_received_confirmation_code),
				R.drawable.blue_dialog_icon_shape, R.drawable.ic_action_accept, buttons, true,null);
		
	}

	private void updateUI (boolean back) {
		if (!back){
			waitProgress.setVisibility(View.VISIBLE);
			chooseCountryItem.setVisibility(View.GONE);
			countryList.setVisibility(View.GONE);
			explationTv.setVisibility(View.INVISIBLE);
			LinearLayout.LayoutParams contParams = (LayoutParams) btnContainer.getLayoutParams();
			contParams.gravity = Gravity.CENTER;
			contParams.rightMargin = 0;
			btnContainer.setLayoutParams(contParams);
			LinearLayout.LayoutParams btnParams = (LayoutParams) sendBtn.getLayoutParams();
			btnParams.gravity = Gravity.CENTER;
			sendBtn.setLayoutParams(btnParams);
			phoneEt.setText("");
			phoneEt.setHint(getString(R.string.set_confirmation_code));
			phoneEt.setGravity(Gravity.CENTER);
			receivedCodeTv.setVisibility(View.VISIBLE);
			receivedCodeTv.setText(getString(R.string.will_received_message));
			changeNumberBtn.setVisibility(View.VISIBLE);
		}else{
			waitProgress.setVisibility(View.GONE);
			chooseCountryItem.setVisibility(View.VISIBLE);
			countryList.setVisibility(View.VISIBLE);
			explationTv.setVisibility(View.VISIBLE);
			LinearLayout.LayoutParams contParams = (LayoutParams) btnContainer.getLayoutParams();
			contParams.gravity = Gravity.RIGHT;
			contParams.rightMargin = 20;
			btnContainer.setLayoutParams(contParams);
			LinearLayout.LayoutParams btnParams = (LayoutParams) sendBtn.getLayoutParams();
			btnParams.gravity = Gravity.LEFT;
			sendBtn.setLayoutParams(btnParams);
			phoneEt.setText("");
			phoneEt.setHint(getString(R.string.set_phone_number));
			phoneEt.setGravity(Gravity.CENTER|Gravity.LEFT);
			receivedCodeTv.setVisibility(View.GONE);
			changeNumberBtn.setVisibility(View.GONE);
		}
		
	}

	public void showVerificationCodeRecived (String code){
		Log.e("codelog", "currentmod - " + currentMood);
		if (code == null || !fragIsOn || currentMood == MOOD_SEND_PHONE){
			return;
		}
		
		receivedCodeTv.setText(code);
		receivedCodeTv.setVisibility(View.VISIBLE);
		waitProgress.setVisibility(View.GONE);
	}

	public void sendConfirmationCode(){
		if (TextUtil.baseFieldsValidation(phoneEt)){
			final ProgressDialog dialog = DialogUtil.showProgressDialog(getActivity(), getString(R.string.deafult_dialog_messgae));
			TipxApp.networkManager.verifyVertificationCode(phoneEt.getText().toString(), new NetworkCallback(){
				
				@Override
				public ResponseObject parseResponse(JSONObject toParse) {
					ResponseObject responseObject = new ResponseObject(toParse);
					return responseObject;
				}
				
				@Override
				public void onDataRecived(ResponseObject response,
						boolean isHasError, String erroDescription) {
					super.onDataRecived(response, isHasError, erroDescription);
					DialogUtil.dismisDialog(dialog);
					Log.i("createuserlog", "response = " + response.getJsonContent());
					if (!isHasError){
						try {
							JSONObject data = new JSONObject(response.getData());
							String userSecret = data.optString("AppuserSecret");
							TipxApp.userProfil.setUserSecret(userSecret);
						} catch (Exception e) {
							
						}
						getUserInfo();
					}else{
						
						ToastUtil.toster(erroDescription, false);
					}
				}
			
				@Override
				public void onError() {
					super.onError();
					DialogUtil.dismisDialog(dialog);
					ToastUtil.toster(getString(R.string.general_network_error), false);
				}
				
				@Override
				public void networkUnavailable(String message) {
					super.networkUnavailable(message);
					DialogUtil.dismisDialog(dialog);
				}
			});
			
		}else{
			ToastUtil.toster(getString(R.string.empty_confirmation_code_), false);
		}
		
	}

	private void resendVerificationCode (final String id) {
		
		final ProgressDialog dialog = DialogUtil.showProgressDialog(getActivity(), getString(R.string.deafult_dialog_messgae));
		TipxApp.networkManager.resendVerificationCode(id, new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				DialogUtil.dismisDialog(dialog);
				Log.i("createuserlog", "response = " + response.getJsonContent());
				if (!isHasError){
					TipxApp.userProfil.setUserId(id);
					currentMood = MOOD_SEND_CODE;
					updateUI(false);
					showWillGetConfirmationCodeDialog();
				}else{
					ToastUtil.toster(erroDescription, false);
				}
				
			}
		
			@Override
			public void onError() {
				super.onError();
				DialogUtil.dismisDialog(dialog);
				ToastUtil.toster(getString(R.string.general_network_error), false);
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				DialogUtil.dismisDialog(dialog);
			}
		});
		
	}
	
	private void getUserInfo(){

		final ProgressDialog dialog = DialogUtil.showProgressDialog(getActivity(), getString(R.string.deafult_dialog_messgae));
		TipxApp.networkManager.getUserInfo(new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				UserProfile responseObject = new UserProfile(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				DialogUtil.dismisDialog(dialog);
				Log.i("createuserlog", "response = " + response.getJsonContent());
				if (!isHasError){
					FragmentsUtil.addFragment(getFragmentManager(), ProfileFragment.newInstance(false), R.id.login_container);
				}else{
					ToastUtil.toster(erroDescription, false);
				}
				
			}
		
			@Override
			public void onError() {
				super.onError();
				DialogUtil.dismisDialog(dialog);
				ToastUtil.toster(getString(R.string.general_network_error), false);
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				DialogUtil.dismisDialog(dialog);
			}
		});
	}


}
