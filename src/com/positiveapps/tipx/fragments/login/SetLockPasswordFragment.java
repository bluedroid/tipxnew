/**
 * 
 */
package com.positiveapps.tipx.fragments.login;



import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.tipx.LoginActivity;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.fragments.BaseFragment;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.ToastUtil;





/**
 * @author natiapplications
 *
 */
public class SetLockPasswordFragment extends BaseFragment implements TextWatcher,OnActionBarButtonClickListener,OnClickListener{
	
	
	private TextView invalidVerfication;
	private EditText passcodeEt;
	private EditText verifyPasscodeEt;
	private Button releaseLockBtn;
	private boolean verifyPasscode;
	
	
	public SetLockPasswordFragment() {

	}

	public static SetLockPasswordFragment newInstance() {
		SetLockPasswordFragment instance = new SetLockPasswordFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_set_lock_password, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		try {
			LoginActivity.currentFragment = this;
			LoginActivity.setScreenTitle(TipxApp.appContext.getResources().getString(R.string.lock_screen_screen_title));
			LoginActivity.removeAllActionBarButtons();
		} catch (Exception e) { 
			MainActivity.currentFragment = this;
			MainActivity.setScreenTitle(TipxApp.appContext.getResources().getString(R.string.lock_screen_screen_title),null,null);
			MainActivity.removeAllActionBarButtons();
		}
		
		
		invalidVerfication = (TextView)view.findViewById(R.id.invlid_verification_txt);
		passcodeEt = (EditText)view.findViewById(R.id.pass_et);
		passcodeEt.requestFocus();
		verifyPasscodeEt = (EditText)view.findViewById(R.id.verify_pass_et);
		releaseLockBtn = (Button)view.findViewById(R.id.release_lock_btn);
		
		releaseLockBtn.setOnClickListener(this);
		verifyPasscodeEt.addTextChangedListener(this);
		passcodeEt.addTextChangedListener(this);
		
		invalidVerfication.setVisibility(View.GONE);
		verifyPasscode = true;
		
		setUpActionBarButtons();
		passcodeEt.setText(TipxApp.userSettings.getLockPasswordNumber());
		verifyPasscodeEt.setText(TipxApp.userSettings.getLockPasswordNumber());
		
	}

	
	/* (non-Javadoc)
	 * @see com.positiveapps.tipx.fragments.BaseFragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		AppUtil.hideKeyBoard(passcodeEt);
		AppUtil.hideKeyBoard(verifyPasscodeEt);
	}
	
	private void setUpActionBarButtons (){
		try {
			LoginActivity.removeAllActionBarButtons();
			ActionBarButton saveBtn = new ActionBarButton(getActivity(), 
					R.drawable.empty, TipxApp.appContext.getString(R.string.save),this);
			LoginActivity.addButtonToActionBar(saveBtn);
		} catch (Exception e) {
			MainActivity.removeAllActionBarButtons();
			ActionBarButton saveBtn = new ActionBarButton(getActivity(), 
					R.drawable.empty, TipxApp.appContext.getString(R.string.save),this);
			MainActivity.addButtonToActionBar(saveBtn);
		}
		
	}

		
		
		
	@Override
	public void afterTextChanged(Editable arg0) {

	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {

	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

		if (passcodeEt.getText().toString()
				.equals(verifyPasscodeEt.getText().toString())) {
			verifyPasscodeEt.setSelected(false);
			invalidVerfication.setVisibility(View.GONE);
			verifyPasscodeEt.setTextColor(getResources().getColor(
					R.color.blue_2));
			verifyPasscode = true;
		} else {
			verifyPasscodeEt.setTextColor(getResources()
					.getColor(R.color.red_3));
			verifyPasscode = false;
		}
	}

	
	
	@Override
	public void onActionBarButtonClick(View v) {
System.out.println("vac " + verifyPasscode);
		if (verifyPasscode) {
			verifyPasscodeEt.setSelected(false);
			System.out.println("vac " + verifyPasscodeEt.getText());
			invalidVerfication.setVisibility(View.GONE);
			String passCode = passcodeEt.getText().toString();
			if (passCode.isEmpty()) {
				ToastUtil.toster("Passcode may not be empty !", false);
				return;
			}else{
				TipxApp.userSettings.setLockPasswordNumber(passCode);
				getFragmentManager().popBackStack();
			}
		} else {
			System.out.println("vac dos");
			verifyPasscodeEt.setSelected(true);
			invalidVerfication.setVisibility(View.VISIBLE);
		}

	}

	
	@Override
	public void onClick(View v) {
		TipxApp.userSettings.setLockPasswordNumber("");
		getFragmentManager().popBackStack();
	}
		
	
	
	

	


}
