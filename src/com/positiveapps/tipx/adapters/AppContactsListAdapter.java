/**
 * 
 */
package com.positiveapps.tipx.adapters;

import java.io.File;
import java.util.ArrayList;

import com.meg7.widget.CircleImageView;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.util.BitmapUtil;
import com.squareup.picasso.Callback;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.preference.PreferenceActivity.Header;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class AppContactsListAdapter extends BaseAdapter {

	
	
	private Activity activity;
	private ArrayList<AppContact> contacts;
	private boolean makeListAsChooser;
	private OnContactSelectedListener listener;
	private OnRemovePressedLestener removeListener;
	private String groupAdminId = "";
	
	public AppContactsListAdapter(Activity activity,
			ArrayList<AppContact> contacts) {
		super();
		this.activity = activity;
		this.contacts = contacts;
	}


	public void setListAsChooser (String groupAdminID,OnContactSelectedListener listener){
		this.listener = listener;
		makeListAsChooser = true;
		this.groupAdminId = groupAdminID;
	}
	
	public void setRemoveListner (OnRemovePressedLestener listener){
		this.removeListener = listener;
	}
	
	@Override
	public int getCount() {
		return contacts.size();
	}

	
	@Override
	public Object getItem(int position) {
		return contacts.get(position);
	}

	
	@Override
	public long getItemId(int position) {
		return 0;
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null){
			convertView = activity.getLayoutInflater().inflate(R.layout.item_list_app_contact, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		final AppContact temp = contacts.get(position);
		holder.name.setText(temp.getContactName());
		holder.phone.setText(temp.getContactStatusLine());
		
		if (!makeListAsChooser){
			if(temp.isHasApplication()){
				holder.hasApp.setVisibility(View.VISIBLE);
			}else{
				holder.hasApp.setVisibility(View.GONE);
			}
			holder.checkBox.setVisibility(View.GONE);
			holder.remove.setVisibility(View.GONE);
			
			if (position == 0){
				holder.header.setVisibility(View.VISIBLE);
				if (!temp.getContactName().isEmpty()){
					holder.letter.setText(Character.toUpperCase(temp.getContactName().charAt(0)) + "");
				}
			}else{
				AppContact previous = contacts.get(position-1);
				if(!temp.getContactName().isEmpty()&&!previous.getContactName().isEmpty()){
					String a = temp.getContactName().charAt(0)+"";
					String b = previous.getContactName().charAt(0)+"";
					if (!a.equalsIgnoreCase(b)){
						holder.header.setVisibility(View.VISIBLE);
						holder.letter.setText(Character.toUpperCase(temp.getContactName().charAt(0)) + "");
					}else{
						holder.header.setVisibility(View.GONE);
					}
				}else{
					holder.header.setVisibility(View.GONE);
				}
			}
			
		}else{
			holder.header.setVisibility(View.GONE);
			holder.phone.setVisibility(View.GONE);
			holder.hasApp.setVisibility(View.GONE);
			if (listener != null){
				holder.checkBox.setVisibility(View.VISIBLE);
				holder.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if (listener != null){
							listener.onContactSelected(isChecked, temp);
						}
					}
				});
			}else{
				holder.checkBox.setVisibility(View.GONE);
			}
			if (removeListener != null&&!TipxApp.userProfil.getUserId().equals(temp.getContactId()+"")){
				
				holder.remove.setVisibility(View.VISIBLE);
				holder.remove.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						removeListener.onRemovePressed(temp);
					}
				});
			}else{
				holder.remove.setVisibility(View.GONE);
			}
			
		}
		if (groupAdminId != null&&!groupAdminId.isEmpty()){
			if (temp.getContactId() != null){
				if (temp.getContactId().equals(groupAdminId)){
					holder.phone.setVisibility(View.VISIBLE);
					holder.phone.setTextColor(Color.RED);
					holder.phone.setText("Group admin");
				}
			}
		}
		
		
		String picturePath = temp.getContactImagePath();
		File file = null;
		if (picturePath != null && !picturePath.isEmpty()){
			file = new File(picturePath);
		}else{
			picturePath = temp.getContactImgaeUrl();
		}
		if (file != null){
			BitmapUtil.loadImageIntoByFile(file, holder.imageView,
					R.drawable.ic_action_person, R.drawable.ic_action_person,
					60, 60, null);
		}else{
			
			BitmapUtil.loadImageIntoByUrl(picturePath, holder.imageView,
					R.drawable.ic_action_person, R.drawable.ic_action_person,
					60, 60, null);
			
		}
		
		
		return convertView;
	}
	
	public class ViewHolder {
		public View view;
		public LinearLayout header;
		public TextView letter;
		public TextView name;
		public TextView phone;
		public CircleImageView imageView;
		public CircleImageView hasApp;
		public CheckBox checkBox;
		public ImageView remove;
		public ViewHolder (View view){
			this.view = view;
			header = (LinearLayout) view.findViewById(R.id.header);
			letter = (TextView)view.findViewById(R.id.letter);
			name = (TextView)view.findViewById(R.id.name_txt);
			phone = (TextView)view.findViewById(R.id.phone_txt);
			imageView = (CircleImageView)view.findViewById(R.id.image);
			hasApp = (CircleImageView)view.findViewById(R.id.has_app);
			checkBox = (CheckBox)view.findViewById(R.id.checkContact);
			remove = (ImageView)view.findViewById(R.id.remove);
		}
		public ImageView getImageView (){
			return (ImageView)view.findViewById(R.id.image);
		}
	}

	
	public interface OnContactSelectedListener {
		public void onContactSelected (boolean selected,AppContact contact);
	}
	public interface OnRemovePressedLestener {
		public void onRemovePressed (AppContact contact);
	}
}
