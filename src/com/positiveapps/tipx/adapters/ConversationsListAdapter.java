/**
 * 
 */
package com.positiveapps.tipx.adapters;

import java.io.File;
import java.util.ArrayList;

import com.meg7.widget.CircleImageView;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.Conversation;
import com.positiveapps.tipx.ui.ChatMessageView;
import com.positiveapps.tipx.util.BitmapUtil;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.preference.PreferenceActivity.Header;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class ConversationsListAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<Conversation> converasations;
	public static int[] statusImages = {R.drawable.confirmation_sent,R.drawable.ic_message_accept,R.drawable.ic_message_readed,
		R.drawable.ic_action_remove,R.drawable.x_confirm};
	
	
	public ConversationsListAdapter(Activity activity,
			ArrayList<Conversation> conver) {
		super();
		this.activity = activity;
		this.converasations = ingnorEmptyConversations(conver);
	}

	private ArrayList<Conversation> ingnorEmptyConversations (ArrayList<Conversation> toFilter){
		ArrayList<Conversation> result = new ArrayList<Conversation>();
		for (int i = 0; i < toFilter.size(); i++) {
			if (toFilter.get(i).getConversationType() == ChatMessage.GROUP_MESSAGE){
				result.add(toFilter.get(i));
			}else{
				if (toFilter.get(i).getMessages() != null && toFilter.get(i).getMessages().size() > 0){
					result.add(toFilter.get(i));
				}
			}
		}
		return result;
	}

	@Override
	public int getCount() {
		return converasations.size();
	}

	
	@Override
	public Object getItem(int position) {
		return converasations.get(position);
	}

	
	@Override
	public long getItemId(int position) {
		return 0;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder = null;
		if (convertView == null){
			convertView = activity.getLayoutInflater().inflate(R.layout.item_list_conversation, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		Conversation temp = converasations.get(position);
		/*if (position %2 > 0){
			holder.sell.setBackgroundResource(R.drawable.list_blue_selector);
		}else{
			holder.sell.setBackgroundResource(R.drawable.list_trans_selector);
		}*/
		
		holder.name.setText(temp.getConversationName());
		holder.lastMessages.setText(temp.getLastMessages());
		holder.lastMessagesDate.setText(temp.getLatMessageDate());
		
		int status = temp.getLastMessageStatus()-2;
		if (status>=0){
			holder.lastStatus.setVisibility(View.VISIBLE);
			holder.lastStatus.setImageResource(statusImages[status]);
		}else{
			holder.lastStatus.setVisibility(View.GONE);
		}
		
		int placeHolder = 0;
		if (temp.getConversationType() == ChatMessage.GROUP_MESSAGE){
			placeHolder = R.drawable.ic_action_group;
		}else{
			placeHolder = R.drawable.ic_action_person;
		}
		
		String picturePath = "";
		if (temp.getConversationType() != ChatMessage.GROUP_MESSAGE){
			AppContact appContact = TipxApp.contactsManager.getContactByContactId(temp.getConversationID());
			if (appContact != null){
				picturePath = appContact.getContactImagePath();
				if (picturePath != null && !picturePath.isEmpty()){
					
				}else{
					picturePath = appContact.getContactImgaeUrl();
				}
			}
		}
		
		File file = null;
		
		if (picturePath.isEmpty()){
			picturePath = temp.getImagePath();
			if (picturePath != null && !picturePath.isEmpty()){
				file = new File(picturePath);
			}else{
				picturePath = temp.getIconUrl();
			}
		}
		
		if (file != null){
			picturePath = temp.getIconUrl();
			BitmapUtil.loadImageIntoByFile(file, holder.image,
					placeHolder,placeHolder,
					60, 60, null);
		}else{
			picturePath = temp.getIconUrl();
			BitmapUtil.loadImageIntoByUrl(picturePath, holder.image,
					placeHolder,placeHolder,
					60, 60, null);
		}
		
		
		if (temp.getUnReadMessagesCount() > 0){
			holder.bage.setVisibility(View.VISIBLE);
			holder.bage.setText(temp.getUnReadMessagesCount()+"");
		}else{
			holder.bage.setVisibility(View.GONE);
		}
		
		return convertView;
	}
	
	public class ViewHolder {
		
		public LinearLayout sell;
		public TextView name;
		public TextView lastMessages;
		public TextView lastMessagesDate;
		public TextView bage;
		public CircleImageView image;
		public ImageView lastStatus;
		
		public ViewHolder (View view){
			sell = (LinearLayout)view.findViewById(R.id.sell);
			name = (TextView)view.findViewById(R.id.name);
			lastMessages = (TextView)view.findViewById(R.id.last_message);
			lastMessagesDate = (TextView)view.findViewById(R.id.last_date);
			bage = (TextView)view.findViewById(R.id.bage);
			image = (CircleImageView)view.findViewById(R.id.image);
			lastStatus = (ImageView)view.findViewById(R.id.last_status);
		}
	}

}
