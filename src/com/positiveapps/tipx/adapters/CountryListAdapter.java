/**
 * 
 */
package com.positiveapps.tipx.adapters;

import java.util.ArrayList;

import com.positiveapps.tipx.R;
import com.positiveapps.tipx.objects.CountryObject;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class CountryListAdapter extends BaseAdapter{
	
	
	
	private Activity activity;
	private ArrayList<CountryObject> countries;
	private ViewHolder viewHolder;
	
	
	
	public CountryListAdapter(Activity activity,
			ArrayList<CountryObject> countries) {
		super();
		this.activity = activity;
		this.countries = countries;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return countries.size();
	}
	
	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return countries.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		
		if (convertView == null){
			convertView = activity.getLayoutInflater().inflate(R.layout.item_list_country, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		CountryObject temp = countries.get(position);
		viewHolder.name.setText(temp.getCountryName());
		viewHolder.flag.setImageDrawable(temp.getCountryFlag());
		
		return convertView;
	}
	
	public class ViewHolder {
		
		TextView name;
		ImageView flag;
		
		public ViewHolder (View view){
			name = (TextView)view.findViewById(R.id.country_tv);
			flag = (ImageView)view.findViewById(R.id.cuntry_flag);
		}
		
	}
	

}
