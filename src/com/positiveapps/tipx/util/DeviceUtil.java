/**
 * 
 */
package com.positiveapps.tipx.util;

import java.util.regex.Pattern;

import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Patterns;

/**
 * @author Nati Gabay
 *
 */
public class DeviceUtil {
	
	public static String getUserEmail(){
		String possibleEmail = "";
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; 
		Account[] accounts = AccountManager.get(TipxApp.appContext).getAccounts();
		for (Account account : accounts) {
		    if (emailPattern.matcher(account.name).matches()) {
		    	possibleEmail = account.name;
		    }
		}
		return possibleEmail;
	}
	
	public static String getDeviceUDID() {

		return Secure.getString(TipxApp.appContext.getContentResolver(),
				Secure.ANDROID_ID);
	}
	
	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	public static String getDeviceVersion () {
		
		String myVersion = "";
		if (android.os.Build.VERSION.RELEASE != null){
			myVersion = android.os.Build.VERSION.RELEASE;
		}
		return myVersion;
	}
	
	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	} 
	
	public static String GetCountryZipCode(Context context){
	    String CountryID="";
	    String CountryZipCode="";

	    TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	    //getNetworkCountryIso
	    CountryID= manager.getSimCountryIso().toUpperCase();
	    String[] rl=context.getResources().getStringArray(R.array.CountryCodes);
	    for(int i=0;i<rl.length;i++){
	        String[] g=rl[i].split(",");
	        if(g[1].trim().equals(CountryID.trim())){
	            CountryZipCode=g[0];
	            break;  
	        }
	    }
	    return CountryZipCode;
	}
	
	public static String GetCountryID(){
	    String CountryID="";
	    TelephonyManager manager = (TelephonyManager) 
	    		TipxApp.appContext.getSystemService(Context.TELEPHONY_SERVICE);
	    CountryID= manager.getSimCountryIso().toUpperCase();
	    if (CountryID.trim().isEmpty()){
	    	CountryID = "IL";
	    }
	    return CountryID;
	}
	

}
