/**
 * 
 */
package com.positiveapps.tipx.util;
import java.util.List;


import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.NotificationActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


/**
 * @author natiapplications
 *
 */
public class NotificationUtil {
	
	public static final int NOTIFICATION_ID = 1;
	
	public static void sendChatNotification(String conversationId) {
		Log.e("notificationlog", "sendChatNotification id = " + conversationId);
		int counter  = TipxApp.appPreference.getUserProfil().getNotificationCounter();
		counter++;
		TipxApp.appPreference.getUserProfil().setNotificationCount(counter);
		
		NotificationManager mNotificationManager = (NotificationManager) 
				TipxApp.appContext.getSystemService(Context.NOTIFICATION_SERVICE);

		

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				TipxApp.appContext).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(TipxApp.appContext.getString(R.string.notification_title).replace("*", counter+""))
				.setStyle(new NotificationCompat.BigTextStyle().bigText
						(TipxApp.appContext.getString(R.string.notification_title)
								.replace("*", counter+"")))
				.setContentText(TipxApp.appContext.getString(R.string.notification_title)
						.replace("*", counter+"")).
								setSound( RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

		Intent intent = new Intent(TipxApp.appContext, NotificationActivity.class);
		intent.putExtra(NotificationActivity.EXTERA_CONVERSATION_ID, conversationId);
		
		PendingIntent contentIntent = PendingIntent.getActivity(TipxApp.appContext, 0,
				intent,Intent.FLAG_ACTIVITY_NEW_TASK);
		if (contentIntent == null){
			Log.e("notificationlog", "pending intent is null");
		}
		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}
	
	public static void updateNotification(){
		
		NotificationManager mNotificationManager = (NotificationManager) 
				MainActivity.mainInstance.getSystemService(Context.NOTIFICATION_SERVICE);
		
		
		int counter  = TipxApp.appPreference.getUserProfil().getNotificationCounter();
		counter--;
		TipxApp.appPreference.getUserProfil().setNotificationCount(counter);
		if (counter <= 0){
			mNotificationManager.cancelAll();
		}else{
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					TipxApp.appContext).setSmallIcon(R.drawable.ic_launcher)
					.setContentTitle(TipxApp.appContext.getString(R.string.notification_title).replace("*", counter+""))
					.setStyle(new NotificationCompat.BigTextStyle().bigText
							(TipxApp.appContext.getString(R.string.notification_title)
									.replace("*", counter+"")))
					.setContentText(TipxApp.appContext.getString(R.string.notification_title)
							.replace("*", counter+""));
			mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
		}
	}
	
	public static void setBadge(Context context, int count) {
	    String launcherClassName = getLauncherClassName(context);
	    if (launcherClassName == null) {
	        return;
	    }
	    Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
	    intent.putExtra("badge_count", count);
	    intent.putExtra("badge_count_package_name", context.getPackageName());
	    intent.putExtra("badge_count_class_name", launcherClassName);
	    context.sendBroadcast(intent);
	}

	public static String getLauncherClassName(Context context) {

	    PackageManager pm = context.getPackageManager();

	    Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_LAUNCHER);

	    List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
	    for (ResolveInfo resolveInfo : resolveInfos) {
	        String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
	        if (pkgName.equalsIgnoreCase(context.getPackageName())) {
	            String className = resolveInfo.activityInfo.name;
	            return className;
	        }
	    }
	    return null;
	}

}
