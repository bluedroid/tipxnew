/**
 * 
 */
package com.positiveapps.tipx.util;





import java.io.File;
import java.util.ArrayList;

import com.positiveapps.tipx.RecordingVideoActivity;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts.Intents;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * @author natiapplications
 *
 */
public class ContentProviderUtil {
	
	
	public static void openLocationChooser (Activity activity,String lat,String lon) {
		Uri location = Uri.parse("geo:0,0?q="+lat + "," + lon);
		 Intent intent = new Intent(Intent.ACTION_VIEW,location);
        activity.startActivity(intent);
	}
	
	public static void openSMSScreen (Activity activity,String message,String address) {
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri data = Uri.parse("sms:"+address);
        intent.setData(data);
        intent.putExtra("address", address);
        intent.putExtra("sms_body", message);
       
        
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
	}
	
	
	public static void openGallery (Fragment fragment,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,
		           android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		
		fragment.startActivityForResult(pickPhoto , requestCode);
	}
	
	public static void openGalleryFromChildFragment (Fragment fragment,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,
		           android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		
		fragment.getParentFragment().startActivityForResult(pickPhoto , requestCode);
	}
	
	
	
	public static void openRecordVideoFromChildFragment(Fragment fragment,
			int requestCode,String fileName) {
		/*Intent intent = new Intent(
				android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
		
		intent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
		intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
	    if (intent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
	    	fragment.getParentFragment().startActivityForResult(intent,requestCode);
	    }*/
		Intent intent = new Intent(fragment.getActivity(),RecordingVideoActivity.class);
		intent.putExtra(RecordingVideoActivity.EXTRA_FILE_NAME, fileName);
		fragment.getParentFragment().startActivityForResult(intent, requestCode);
		
	}

	public static void openRecordAudioFromChildFragment(Fragment fragment,
			int requestCode) {
		Intent intent = new Intent(
				android.provider.MediaStore.Audio.Media.RECORD_SOUND_ACTION);
		fragment.getParentFragment().startActivityForResult(intent,requestCode);

	}

	public static void openCameraFromChildFragment(Fragment fragment,
			int requestCode,Uri uri) {
		Intent intent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		if (uri != null){
			intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		}
		fragment.getParentFragment().startActivityForResult(intent,requestCode);
	}
	
	
	
	public static void openAudioGalleryFromChildFragment (Fragment fragment,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,
		           android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
		
		fragment.getParentFragment().startActivityForResult(pickPhoto , requestCode);
	}
	
	public static void openVideoGalleryFromChildFragment (Fragment fragment,int requestCode){
		Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
		mediaChooser.setType("video/*, images/*");
		fragment.getParentFragment().startActivityForResult(mediaChooser , requestCode);
	}
	
	public static void playMediaByUrl (Fragment fragment,String type,String url){
		if (url == null){
			return;
		}
		Log.i("onitemclicklog", "url = " + url);
		try {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.parse(url),type);
			fragment.getParentFragment().startActivity(intent);
		} catch (Exception e) {
			try {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				Uri uri = Uri.fromFile(new File(url));
				intent.setDataAndType(uri,type);
				fragment.getParentFragment().startActivity(intent);
			} catch (Exception e2) {
				ToastUtil.toster("Can not playing this file. file path:" + url, true);
			}
			
		}
	}
	
	public static void playMediaByFilePath (Fragment fragment,String type,String filePath){
		try {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			Uri uri = Uri.fromFile(new File(filePath));
			intent.setDataAndType(uri,type);
			fragment.getParentFragment().startActivity(intent);
		} catch (Exception e2) {
			ToastUtil.toster("Can not playing this file. file path:" + filePath, true);
		}
	}
	
	public static void openImageInDevice (Fragment fragment,String type,String url){
		if (url == null){
			return;
		}
		Log.i("onitemclicklog", "url = " + url);
		try {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.parse(url),type);
			fragment.startActivity(intent);
		} catch (Exception e) {
			try {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				Uri uri = Uri.fromFile(new File(url));
				intent.setDataAndType(uri,type);
				fragment.startActivity(intent);
			} catch (Exception e2) {
				ToastUtil.toster("Can not playing this file. file path:" + url, true);
			}
			
		}
	}
	
	public static void deleteFileFromSdcard (final String filePath){
		if (filePath == null||filePath.isEmpty()){
			return;
		}
		try {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					File file = new File(filePath);
					if (file != null){
						if (file.exists()){
							boolean deleted = file.delete();	
						}
					}
					
				}
			}).start();
			
		} catch (Exception e) {
			ToastUtil.toster("Can not delete this file. path:" + filePath, true);
		}
		
	}
	
	public static void openCamera (Fragment fragment){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.startActivity(takePicture);
	}
	
	public static void openCameraForResult (Fragment fragment,int requestCode){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.startActivityForResult(takePicture,requestCode);
	}
	
	public static void openCameraForResultWithUri (Fragment fragment,int requestCode,Uri uri){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePicture.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		fragment.startActivityForResult(takePicture,requestCode);
	}
	
	public static void openImageInGallery (Activity activity,String phth) {
		Intent intent = null;
		intent = new Intent(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://"+phth), "image/*");
		activity.startActivity(intent);
	}
	
	public static void mackCallPhone (Fragment fragment,String phone) {
		
		Uri callUri = Uri.parse("tel://" + phone);
		Intent callIntent = new Intent(Intent.ACTION_CALL,callUri);
		callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_USER_ACTION);
		fragment.startActivity(callIntent);
	}
	
	public static void openGPSSettings (Fragment fragment) {
		fragment.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettings (Activity activiy) {
		activiy.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettingsForResult (Activity activity,int requestCode) {
		activity.startActivityForResult(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS),requestCode);
	}
	
	public static void openGPSSettingsForResult (Fragment fragment,int requestCode) {
		fragment.startActivityForResult(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS),requestCode);
	}
	
	public static void openBrowser (Fragment fragment,String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		fragment.getActivity().startActivity(browserIntent);
	}
	
	public static void openBrowser (Activity activity,String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		activity.startActivity(browserIntent);
	}
	
	public static void openWaze (Activity activity,String address) {
		
		try
		{
			Uri location = Uri.parse("waze://?q="+address);
			Intent intent = new Intent(Intent.ACTION_VIEW,location);
			activity.startActivity(intent);
		}
		catch ( Exception ex  )
		{
		   Intent intent =
		    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
		   activity.startActivity(intent);
		}
	}
	
	public static void openContactListToChoose (Fragment fragment,int requestCode) {
		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_LOOKUP_URI);
		intent.setType(android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
	    fragment.startActivityForResult(intent, requestCode);
	}
	
	public static AppContact getContactDetailseByUri (Uri uri){
		Cursor dataCursor = TipxApp.appContext.getContentResolver().query(uri, null, null,
 				null, null);
 		AppContact rezult = new AppContact();
 		 if (dataCursor.moveToNext()){

 			String displayName = dataCursor.getString(dataCursor
 					.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
 			String phoneNumber = dataCursor
 					.getString(dataCursor
 							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA1));
 			rezult.setContactName(displayName);
 			String num = TextUtil.normalizePhoneNumber(phoneNumber);
 			if (num == null){
 				return null;
 			}
 			rezult.setContactPhone(num);
 		}
 		 return rezult;
	}
	
public static ArrayList<AppContact> getAllContactsFromPhone(){
		
		
		ArrayList<AppContact> friends = new ArrayList<AppContact>();
		
		ContentResolver cr = TipxApp.appContext.getContentResolver();  
	    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, Phone.DISPLAY_NAME + " ASC");
	    if(cursor.moveToFirst())
	    {   
	        do
	        { 
	            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
	            if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
	            {
	            	AppContact friend = new AppContact();
	            	String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
	            	friend.setContactName(displayName);

		        	 
	                Cursor pCur = cr.query(Phone.CONTENT_URI,null,Phone.CONTACT_ID +" = ?",new String[]{ id }, null);
	                while (pCur.moveToNext()) 
	                {
	                 
	                	String friendNumber = null;
	                	System.out.println("------------------------> " + displayName);
	                	System.out.println("------------------------> " + pCur.getString(pCur.getColumnIndex(Phone.NUMBER)));
	                	//friendNumber = friendNumber.replaceAll("-", "");
	                	System.out.println("");
	                	
	                	friendNumber = pCur.getString(pCur.getColumnIndex(Phone.NUMBER));
                    	friendNumber = TextUtil.normalizePhoneNumber(friendNumber);
                    	System.out.println("-----------------MOBILE------> " + pCur.getString(pCur.getColumnIndex(Phone.NUMBER)));
                    	Log.d("normalizePhoneLog", "current phone: " + friendNumber);
                    	if (friendNumber == null){
	                		Log.e("normalizePhoneLog", "phone is not valid !");
	                		friendNumber = "";
	                	}else{
	                		
	                		friendNumber = friendNumber.replace("+", "");
	                        if(friendNumber.substring(0, 3).matches("9720")){
	                        	friendNumber = TextUtil.removeCharAt(friendNumber, 3);
	                        }
	                        
	                	}
	                	Log.i("normalizePhoneLog", "apter norm: " + friendNumber);
	                	friend.setContactPhone(friendNumber); 
	                }
	                
	            
					
					if(friend.getContactPhone() != null){
						friends.add(friend);
					}
					
	                pCur.close();
	            }
	            
	            
	        } while (cursor.moveToNext()) ;
	    }
	    cursor.close(); 
	    
		return friends;
	   
	}

 


}
