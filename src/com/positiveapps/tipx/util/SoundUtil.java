/**
 * 
 */
package com.positiveapps.tipx.util;

import com.positiveapps.tipx.TipxApp;

import android.media.MediaPlayer;

/**
 * @author natiapplications
 *
 */
public class SoundUtil {
	
	public static void playSound(int soundFileId) {
		MediaPlayer mediaPlayer = MediaPlayer.create(TipxApp.appContext,
				soundFileId);
		mediaPlayer.start();
	}

}
