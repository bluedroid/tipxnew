/**
 * 
 */
package com.positiveapps.tipx.gcm;

import java.util.Set;

import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.util.DateUtil;




/**
 * @author Nati Application
 *
 */
public class GcmIntentService extends IntentService {
	
	
	public static final int NOTIFICATION_ID = 1;
	
	
	private final String CODE = "code";					
	private final String MESSAGE_ID = "message_id";			
	private final String MESSAGE = "message"; 				
	private final String LAT = "lat";					
	private final String LNG = "lng";					
	private final String GROUP_ID = "to_group_id";			
	private final String GROUP_NAME = "to_group_name";			
	private final String GROUP_CODE = "to_group_code";			
	private final String GROUP_ICON = "to_group_icon";			
	private final String GROUP_ACCESS_LEVEL = "to_group_is_public";	
	private final String GROUP_ADMIN_ID = "to_group_admin_id";		
	private final String GROUP_CREATED_AT = "to_group_creates";		
	private final String MEMBER_ID = "new_member_id";		
	private final String MEMBER_FIRST_NAME = "new_member_first_name";	
	private final String MEMBER_LAST_NAME = "new_member_last_name";	
	private final String USER_ID = "to_user_id";			
	private final String FROM_USER = "from_user";
	private final String SENDER_ID = "id";			
	private final String SENDER_FBID = "fbid";
	private final String FIRST_NAME = "first_name";	
	private final String LAST_NAME = "last_name";	
	private final String SENDER_PHONE = "phone";
	private final String MEDIA_URL = "media_url";
	private final String AUTO_DELETE = "auto_delete";
	private final String FROM_ID = "from_id";
	private final String DATE = "date";
			
	
	NotificationCompat.Builder builder;
	
	

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);
		Log.e("receive message 2","********************"+ messageType +"*************************");
		Log.e("receive message 2","extra is null ? "+ (extras == null) +"*************************");
		try {
			if (extras != null){
				handelNotification(extras);
				Set<String> keys = extras.keySet();
				for (String key:keys) {
					Log.e("receive message 2","messgae_code = "+key + " --- " + extras.get(key));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void handelNotification (Bundle extras){
		try {
			String messgae_code = extras.getString(CODE);
			String message_id = extras.getString(MESSAGE_ID);
			String messgae_content = extras.getString(MESSAGE);
			String group_id = extras.getString(GROUP_ID);
			String userId = extras.getString(USER_ID);
			String lat = extras.getString(LAT);
			String lng = extras.getString(LNG);
			String from_user = extras.getString(FROM_USER);
			String mediaUrl = extras.getString(MEDIA_URL);
			String autoDelete = extras.getString(AUTO_DELETE);
			String fromId = extras.getString(FROM_ID);
			String date = extras.getString(DATE);
			Log.e("aoutodlele", "autodlelete = " + autoDelete);
			
			JSONObject fromUserJsonObject = null;
			
			
			ChatMessage temp = new ChatMessage();
			if (autoDelete != null){
				temp.setAutoDelete(!autoDelete.equals("0"));
			}
			
			
			temp.setMessagePhat(ChatMessage.PHAT_FROM);
			temp.setSide(ChatMessage.SIDE_LEFT);
			if (message_id != null && !message_id.isEmpty()){
				temp.setMessageId(Integer.parseInt(message_id));
			}
			if (messgae_code != null && !messgae_code.isEmpty()){
				temp.setType(Integer.parseInt(messgae_code));
			}
			if (group_id != null && !group_id.isEmpty()){
				temp.setGroupId(Integer.parseInt(group_id));
				temp.setChatType(ChatMessage.GROUP_MESSAGE);
			}
			if (userId != null && !userId.isEmpty()){
				temp.setSenderId(userId);
				temp.setChatType(ChatMessage.PRIVATE_MESSAGE);
			}
			if (lat != null && !lat.isEmpty()){
				temp.setLat(Double.parseDouble(lat));
			}
			if (lng != null && !lng.isEmpty()){
				temp.setLng(Double.parseDouble(lng));
			}
			temp.setLink(mediaUrl);
			temp.setContent(messgae_content);
			temp.setFilePath(messgae_content);
			
			//TODO change created time stamp
			Log.d("messageDateLog", "date = " + date);
			temp.setCreated(date);
			
			if (from_user != null && !from_user.isEmpty()){
				fromUserJsonObject = new JSONObject(from_user);
				temp.setSenderId(fromUserJsonObject.optString(SENDER_ID));
				temp.setUserName(fromUserJsonObject.optString("username"));
				temp.setSenderPhone(fromUserJsonObject.optString(SENDER_PHONE));
			}
			if (fromId != null&&!fromId.isEmpty()){
				temp.setSenderId(fromId);
			}
			if (TipxApp.smackManager != null){
				Log.e("notificationLog", "XMPP is connected ? " + TipxApp.smackManager.isConnected());
			}
			
			if (!MainActivity.activityISsOn){
				TipxApp.chatManager.handleReceivedMessage(temp);
				Log.e("chatmessageparserlog", "listener is  null");
				if (!TipxApp.contactsManager.isContactHiden(temp.getSenderPhone())){
					TipxApp.chatManager.notifyUserNewChatMessage(temp);
				}
				/*if (temp.isAutoDelete()){
					temp.startRemoveTimer();
				}*/
			}else{
				if (!TipxApp.smackManager.isConnected()){
					TipxApp.chatManager.handleReceivedMessage(temp);
					Log.e("chatmessageparserlog", "listener is  null");
					if (MainActivity.tipxChatListener != null) {
						if (!TipxApp.contactsManager.isContactHiden(temp.getSenderPhone())){
							MainActivity.tipxChatListener.onChatMessageReceived(temp);
						}
					}else{
						if (!TipxApp.contactsManager.isContactHiden(temp.getSenderPhone())){
							TipxApp.chatManager.notifyUserNewChatMessage(temp);
						}
					}
					if (MainActivity.conversationListener != null){
						MainActivity.conversationListener.onChatMessageReceived(temp);
					}
					if (temp.isAutoDelete()){
						Log.e("autoDeletelog", "is autoDelete ? " + temp.isAutoDelete());
						temp.startRemoveTimer();
					}
				}
			}
			
		} catch (Exception e) {
			Log.e("receive message 2","parse messgae ex"+ e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	

	

}
