/**
 * 
 */
package com.positiveapps.tipx.ui;

import java.util.ArrayList;

import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;

import android.R.integer;
import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

/**
 * @author natiapplications
 *
 */
public class PagerTabIndicator implements OnClickListener {
	
	
	private Activity activity;
	private View view;
	private ArrayList<String> titles;
	private ArrayList<LinearLayout> indicators = new ArrayList<LinearLayout>();
	private ArrayList<TextView> texts = new ArrayList<TextView>();
	private ArrayList<FrameLayout> items = new ArrayList<FrameLayout>();
	private int position;
	private ViewPager pager;
	HorizontalScrollView scrollContainer;
	private onTabSelectedListener listener;
	
	public PagerTabIndicator (Activity activity,ViewPager pager,ArrayList<String> titles,int position,onTabSelectedListener listener){
		this.activity = activity;
		this.titles = titles;
		this.position = position;
		this.pager = pager;
		this.listener = listener;
		buildView();
	}


	/**
	 * 
	 */
	private void buildView() {
		view = activity.getLayoutInflater().inflate(R.layout.pager_tab_indicator, null);
		scrollContainer = (HorizontalScrollView)view.findViewById(R.id.indicator_scroll);
		LinearLayout container = (LinearLayout)view.findViewById(R.id.container);
		
		int width = TipxApp.generalSettings.getScreenWidth();
		if (titles.size() == 2){
			width = width/2;
		}else if (titles.size() > 2){
			width = width/3;
		}
		
		for (int i = 0; i < titles.size(); i++) {
			View tab = activity.getLayoutInflater().inflate(R.layout.layout_chat_tab, null);
			
			tab.setLayoutParams(new LinearLayout.LayoutParams(width,100));
			FrameLayout item = (FrameLayout) tab.findViewById(R.id.item);
			TextView title = (TextView)tab.findViewById(R.id.title);
			LinearLayout indicator = (LinearLayout)tab.findViewById(R.id.line);
			indicators.add(indicator);
			texts.add(title);
			items.add(item);
			item.setTag(new Integer(i));
			item.setOnClickListener(this);
			title.setText(titles.get(i));
			container.addView(tab);
		}
		setPosition(position);
	}

	public void setPosition (int position){
		if (position > items.size()-1){
			return;
		}
		for (int i = 0; i < items.size(); i++) {
			if (i == position){
				texts.get(i).setTextColor(activity.getResources().getColor(R.color.blue_3));
				indicators.get(i).setSelected(true);
			}else{
				texts.get(i).setTextColor(activity.getResources().getColor(R.color.grey2));
				
				indicators.get(i).setSelected(false);
				
			}
		}
		try {
			scrollToSelelctedPosition(position);
			pager.setCurrentItem(position);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public View getView(){
		return this.view;
	}
	
	@Override
	public void onClick(View v) {
		int pos = ((Integer)v.getTag()).intValue();
		setPosition(pos);
		this.position = pos;
		if (listener != null){
			listener.onTabSelected(this, pos);
		}
	}


	private void scrollToSelelctedPosition (int position){
		try {
			int factor = (items.get(0).getWidth()*position) - (items.get(0).getWidth());
			animateScrollingLayoutToBottom(factor);
		} catch (Exception e) {}
		
	}
	
	private void animateScrollingLayoutToBottom(final int factor) {
		
		scrollContainer.postDelayed(new Runnable() {
			public void run() {
				scrollContainer.smoothScrollTo(factor, 0);
			}
		}, 0);
	}
	

	public interface onTabSelectedListener {
		public void onTabSelected(PagerTabIndicator tab,int position);
	}
}
