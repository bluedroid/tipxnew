/**
 * 
 */
package com.positiveapps.tipx.ui;




import com.positiveapps.tipx.TipxApp;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;

/**
 * @author Nati Gabay
 *
 */
public class BubbleMessageBoxLeft extends Drawable {

	private static final int OFFSET = 15;
	Paint whitePaint = new android.graphics.Paint();
	
	private int color;
	public BubbleMessageBoxLeft(int color){
		this.color = color;
	}
	
	
	@Override
	public void setColorFilter(ColorFilter cf) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setAlpha(int alpha) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int getOpacity() {
		// TODO Auto-generated method stub
		return android.graphics.PixelFormat.OPAQUE;
	}
	
	@Override
	public void draw(Canvas canvas) {
		 Rect r = getBounds();
		 RectF rect = new RectF(r);
		 rect.inset(OFFSET, OFFSET);
		 
		 //Build a path
		 Path path = new Path();

		 //up arrow
		/* path.moveTo(OFFSET, OFFSET);*/
		 path.moveTo(OFFSET, 0);
		 
		 //top horizontal line.
		/* path.lineTo(r.width() - OFFSET*3, OFFSET);*/
		 
		 path.lineTo((r.width() - OFFSET) - (r.height()/2), 0);
		 
		 
		 //top right arc
		/* path.arcTo(new RectF(r.right - OFFSET*3, OFFSET, r.right-OFFSET, OFFSET*3), 270, 90);*/
		 
		 path.arcTo(new RectF((r.width() - OFFSET)-(r.height()/2), 0, r.right, r.height()/2), 270, 90);
		 
		 //right vertical line.
		 /*path.lineTo(r.width()-OFFSET, r.height()-OFFSET*2);*/
		 
		 //bottom right arc.
		/* path.arcTo(new RectF(r.right - OFFSET * 3, r.bottom- OFFSET * 2, r.right-OFFSET, r.bottom), 0, 90);*/
		 path.arcTo(new RectF(r.width() - (r.height()/2) , r.height()/2, r.right, r.bottom), 0, 90);
		 
		 //bottom horizontal line.
		/* path.lineTo(OFFSET*3, r.height());*/
		 
		 path.lineTo((r.height()/2)+(OFFSET/2) , r.height());
		 
		 //bottom left arc.
		/* path.arcTo(new RectF(OFFSET, r.bottom- OFFSET*2 , OFFSET * 3, r.bottom), 90, 90);*/
		 
		 path.arcTo(new RectF(OFFSET, ( (r.height()/2) + (OFFSET/2) ) , 
				 (r.height()/2)+(OFFSET/2), r.bottom), 90, 90);
		 
		 //left horizontal line.
		 /*path.lineTo( OFFSET, r.height() - OFFSET);
		 path.lineTo(0, r.height() - OFFSET*2);
		 path.lineTo( OFFSET, r.height() - OFFSET*3);
		 path.lineTo(OFFSET, OFFSET*2);*/
		 
		 path.lineTo( OFFSET, (r.height()/2) + OFFSET);
		 path.lineTo(0, r.height()/2);
		 path.lineTo( OFFSET, r.height()/2 - OFFSET);
		 
		 
		 //top right arc.
		 path.arcTo(new RectF(OFFSET, 0,
				(r.height()/2) + (OFFSET/2),
				 (r.height()/2) + (OFFSET/2) ), 180, 90);
		 
		 path.close();
		 whitePaint.setColor(TipxApp.appContext.getResources().getColor(color));
		 whitePaint.setStyle(Style.FILL);
		 canvas.drawPath(path, whitePaint);
	}

}
