package com.positiveapps.tipx.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * 
 * @author Maor
 *
 */

public class MessageBubbleView extends FrameLayout {

	public static final int INCOMING_MESSAGE = 1;
	public static final int OUTGOING_MESSAGE = 2;
	
	private static final float cornersRadius = 10;
	private static final float spaceFromStart = 20;
	private static final float pointerStartPoint = 40;
	private static final float pointerHeight = 40;
	
	//private static final int MINIMUM_WIDTH = (int) (spaceFromStart * 5);
	//private static final int MINIMUM_HEIGHT = (int) (pointerStartPoint * 2);
	
	private int messageType = OUTGOING_MESSAGE;
	
	/**
	 * @param context
	 */
	public MessageBubbleView(Context context) {
		super(context);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public MessageBubbleView(Context context, AttributeSet attrs) {
		super(context, attrs);		
	}



	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {		
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
/*		if(widthMeasureSpec < MINIMUM_WIDTH){
			getLayoutParams().width = MINIMUM_WIDTH;
		}
		if(heightMeasureSpec < MINIMUM_HEIGHT){
			getLayoutParams().height = MINIMUM_HEIGHT;
		}*/
	}
	
	
	@Override
	protected void dispatchDraw(Canvas canvas) {
		RectF rect;
		Path path = new Path();

		if(messageType == OUTGOING_MESSAGE){
			rect = new RectF(spaceFromStart, 0, getWidth(), getHeight());;
			path.moveTo(0, pointerStartPoint);
			path.lineTo(spaceFromStart, pointerStartPoint - (pointerHeight/2));
			path.lineTo(spaceFromStart, pointerStartPoint + (pointerHeight/2));
			path.lineTo(0, pointerStartPoint);
			path.addRoundRect(rect, cornersRadius, cornersRadius, Path.Direction.CW);
			
		}else{
			int xSpaceFromStartPoint = (int) (getWidth() - spaceFromStart);
			rect = new RectF(0, 0, getWidth() - spaceFromStart, getHeight());			
			path.moveTo(getWidth(), pointerStartPoint);
			path.lineTo(xSpaceFromStartPoint, pointerStartPoint - (pointerHeight/2));
			path.lineTo(xSpaceFromStartPoint, pointerStartPoint + (pointerHeight/2));
			path.lineTo(getWidth(), pointerStartPoint);
			path.addRoundRect(rect, cornersRadius, cornersRadius, Path.Direction.CW);
		}
		
		path.close();
		canvas.clipPath(path);
		super.dispatchDraw(canvas);
	}

	
	
	//-----------------------GET and SET METHODS-------------------------------------------
	/**
	 * @return the messageType
	 */
	public int getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}


}
