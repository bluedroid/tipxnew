package com.positiveapps.tipx;



import com.positiveapps.tipx.objects.Conversation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


public class NotificationActivity extends Activity {

	public static final String EXTERA_CONVERSATION_ID = "ConversationId";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		
		Log.i("notificationlog", "on Notification activity created");
		
		checkForNotifications();
	}

	
	private void checkForNotifications() {
		
		if (getIntent() != null) {
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				final String conversationID = extras.getString(EXTERA_CONVERSATION_ID);
				Conversation conversation = TipxApp.
						conversationManager.openConversationWithConversationID(conversationID);
				if (conversation != null) {
					MainActivity.isFromNotification = true;
					MainActivity.notificationConversation = conversation;
				}else{
					MainActivity.isFromNotification = false;
				}
				end();
			} else {
				MainActivity.isFromNotification = false;
				end();
			}

		}
	}

	
	private void end (){
		Log.e("notificationlog", "on Notification activity Ended. main activity is active ? " + MainActivity.activityISsOn);
		if (!MainActivity.activityISsOn) {
			
			Intent intent = new Intent(NotificationActivity.this,
					SplashActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
		finish();
	}
	
	
}
