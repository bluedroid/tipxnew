package com.positiveapps.tipx;

import com.positiveapps.tipx.fragments.ChoosMembersFragment;
import com.positiveapps.tipx.fragments.login.RegistrationFragment;
import com.positiveapps.tipx.objects.GroupObject;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.FragmentsUtil;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PostActivity extends FragmentActivity {

    public static final String 	EXTRA_TYPE = "Type";
    public static final String 	EXTRA_GROUP_OBJECT = "GroupObject";
    
    public static final int TYPE_MEMBER_CHOOSER = 3;
    
	
	private boolean activityIsOn; 
	public static Fragment currentFragment;
	
	public static TextView screenTitle;
	public static LinearLayout buttonsContainer;
	public static int currentType;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeNoTitleActivity(this);
		setContentView(R.layout.activity_post);
		
		screenTitle = (TextView)findViewById(R.id.screen_title);
		buttonsContainer = (LinearLayout)findViewById(R.id.action_bar_buttons_container);
		
		activityIsOn = true;
		if (getIntent().getExtras() != null){
			currentType = getIntent().getExtras().getInt(EXTRA_TYPE);
			addCorrectFragment ();
		}
		
	}
    
	/**
	 * 
	 */
	private void addCorrectFragment() {
		switch (currentType) {
		case TYPE_MEMBER_CHOOSER:
			GroupObject groupObject = (GroupObject)getIntent().getExtras().getSerializable(EXTRA_GROUP_OBJECT);
			FragmentsUtil.addFragment(getSupportFragmentManager(),
							ChoosMembersFragment.newInstance(groupObject), R.id.post_container);
			break;
		}
	
		
	}

	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		activityIsOn = false;
	}
	
	
	public static void setScreenTitle (String text){
		if (text != null){
			screenTitle.setText(text);
		}else{
			screenTitle.setText(TipxApp.appContext.getString(R.string.app_title));
		}
	}
	
	public static void addButtonToActionBar (ActionBarButton barButton){
		buttonsContainer.addView(barButton.getView());
	}
	
	public static void removeAllActionBarButtons(){
		buttonsContainer.removeAllViews();
	}
	
	public static void removeButtonFromActionBar (View toRemove){
		buttonsContainer.removeView(toRemove);
	}
	
	
}