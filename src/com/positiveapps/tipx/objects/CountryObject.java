/**
 * 
 */
package com.positiveapps.tipx.objects;

import java.io.Serializable;

import org.xbill.DNS.SRVRecord;

import android.graphics.drawable.Drawable;

/**
 * @author natiapplications
 *
 */
public class CountryObject {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String countryName;
	private String countryCode;
	private Drawable countryFlag;
	
	
	public CountryObject(String countryName, String countryCode,
			Drawable countryFlag) {
		super();
		this.countryName = countryName;
		this.countryCode = countryCode;
		this.countryFlag = countryFlag;
	}


	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}


	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}


	/**
	 * @return the countryFlagResource
	 */
	public Drawable getCountryFlag() {
		return countryFlag;
	}
	
	

}
