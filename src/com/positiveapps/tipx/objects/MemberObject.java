/**
 * 
 */
package com.positiveapps.tipx.objects;

import java.io.Serializable;

import org.json.JSONObject;

import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;

/**
 * @author natiapplications
 *
 */
public class MemberObject implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String FIRAT_NAME = "FirstName";
	private final String LAST_NAME = "LastName";
	private final String EMAIL = "Email";
	private final String ID = "ID";
	private final String FBID = "FBID";
	private final String FBAVATAR = "FBAvatar";
	private final String USER_NAME = "Username";
	private final String ONLINE = "Online";
	
	private String firstName;
	private String lastName;
	private String email;
	private int id;
	private String fbid;
	private String fbAvatar;
	private String userName;
	private String onLine;
	private String contactName;
	
	public MemberObject (JSONObject toParse){
		
		this.firstName = toParse.optString(FIRAT_NAME);
		this.lastName = toParse.optString(LAST_NAME);
		this.email = toParse.optString(EMAIL);
		this.id = toParse.optInt(ID);
		this.fbid = toParse.optString(FBID);
		this.fbAvatar = toParse.optString(FBAVATAR);
		this.userName = toParse.optString(USER_NAME);
		this.onLine = toParse.optString(ONLINE);
		AppContact appContact = TipxApp.contactsManager.getContactByContactId(this.id+"");
		if (appContact != null){
			this.contactName = appContact.getContactName();
		}
		
	}


	public  MemberObject (AppContact source){
			this.id = Integer.parseInt(source.getContactId());
			this.userName = source.getContactName();
			this.firstName = "";
			this.lastName = "";
			this.email = "";
			this.fbid = "";
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}


	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the fbid
	 */
	public String getFbid() {
		return fbid;
	}


	/**
	 * @param fbid the fbid to set
	 */
	public void setFbid(String fbid) {
		this.fbid = fbid;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		if (this.contactName != null&&!this.contactName.isEmpty()){
			return this.contactName;
		}
		String result = "";
		if (!getFirstName().isEmpty()){
			result += getFirstName();
		}
		if (!getLastName().isEmpty()){
			result += getLastName();
		}
		if (result.isEmpty()){
			result = userName;
		}
		return result;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * @return the onLine
	 */
	public String getOnLine() {
		return onLine;
	}


	/**
	 * @param onLine the onLine to set
	 */
	public void setOnLine(String onLine) {
		this.onLine = onLine;
	}
	
	
  

}
