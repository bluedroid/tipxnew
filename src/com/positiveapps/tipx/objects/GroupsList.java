/**
 * 
 */
package com.positiveapps.tipx.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.positiveapps.tipx.network.ResponseObject;




/**
 * @author natiapplications
 *
 */
public class GroupsList extends ResponseObject implements Serializable{
	
	
	
	private final String GROUPS = "Groups";
	
	private ArrayList<GroupObject> groupsArray =  new ArrayList<GroupObject>();
	
	
	private static final long serialVersionUID = 1L;
	
	
	

	
	public GroupsList(JSONObject toParse) {
		super(toParse);
		try {
			JSONObject data = new JSONObject(getData());
			JSONArray groups = data.optJSONArray(GROUPS);
			for (int i = 0; i < groups.length(); i++) {
				GroupObject temp = new GroupObject(groups.optJSONObject(i),true,i+"");
				groupsArray.add(temp);
			}
			
			Collections.sort(groupsArray, new Comparator<GroupObject>() {

				@Override
				public int compare(GroupObject lhs, GroupObject rhs) {
					return rhs.getCreatedAtMillis().compareTo(lhs.getCreatedAtMillis());
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("createExlog", "ex - " + e.getMessage());
		}
	}

	


				/**
	 * @return the groupsArray
	 */
	public ArrayList<GroupObject> getGroupsArray() {
		return groupsArray;
	}




	/**
	 * @param groupsArray the groupsArray to set
	 */
	public void setGroupsArray(ArrayList<GroupObject> groupsArray) {
		this.groupsArray = groupsArray;
	}


	

}
