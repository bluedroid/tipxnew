/**
 * 
 */
package com.positiveapps.tipx.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.util.DateUtil;

import android.util.Log;


/**
 * @author natiapplications
 *
 */
public class Conversation implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int conversationType;
	private String conversationID;
	private ArrayList<ChatMessage> messages = new ArrayList<ChatMessage>();
	private GroupObject groupObject;
	private String conversationName;
	private String iconUrl;
	private String imagePath;
	private int postion;
	private int lastvisableMessagesCount;
	private int color;
	private boolean needUpdate;
	private boolean backgroundIsImage;
	private String backroundPath;
	public Conversation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Conversation(int conversationType, String conversationID,
			ArrayList<ChatMessage> messages, String conversationName,String iconUrl,String imagePath
			,int color,String bgPath,boolean isBgImage) {
		super();
		this.conversationType = conversationType;
		this.conversationID = conversationID;
		this.messages = messages;
		this.conversationName = conversationName;
		this.iconUrl = iconUrl;
		this.imagePath = imagePath;
		this.color = color;
		this.backgroundIsImage = isBackgroundIsImage();
		this.backroundPath = bgPath;
		
	}
	
	public Conversation(int conversationType, String conversationID,
			ArrayList<ChatMessage> messages, String conversationName,String iconUrl,String imagePath
			,int color,GroupObject groupObject,String bgPath,boolean isBgImage) {
		super();
		this.conversationType = conversationType;
		this.conversationID = conversationID;
		this.messages = messages;
		this.conversationName = conversationName;
		this.iconUrl = iconUrl;
		this.imagePath = imagePath;
		this.color = color;
		this.groupObject = groupObject;
		this.backgroundIsImage = isBackgroundIsImage();
		this.backroundPath = bgPath;
		
	}
	
	
	public void updateConversaionPropertyByContact (AppContact contact){
		this.conversationName = contact.getContactName();
		this.iconUrl = contact.getContactImgaeUrl();
		this.imagePath = contact.getContactImagePath();
		this.color = contact.getContactColor();
		this.backgroundIsImage = contact.isBackgroundIsImage();
		this.backroundPath = contact.getBackgroundPath();
		
	}
	
	public void updateConversaionPropertyByGroup (GroupObject group){
		this.conversationName = group.getName();
		this.iconUrl = group.getIcon();
		this.imagePath = group.getImagePath();
		this.color = group.getGroupColor();
		this.groupObject = group;
		this.backgroundIsImage = group.isBackgroundIsImage();
		this.backroundPath = group.getBackgroundPath();
	}
	/**
	 * @return the conversationType
	 */
	public int getConversationType() {
		return conversationType;
	}
	/**
	 * @param conversationType the conversationType to set
	 */
	public void setConversationType(int conversationType) {
		this.conversationType = conversationType;
	}
	/**
	 * @return the conversationID
	 */
	public String getConversationID() {
		return conversationID;
	}
	
	
	/**
	 * @return the backgroundIsImage
	 */
	public boolean isBackgroundIsImage() {
		return backgroundIsImage;
	}
	/**
	 * @param backgroundIsImage the backgroundIsImage to set
	 */
	public void setBackgroundIsImage(boolean backgroundIsImage) {
		this.backgroundIsImage = backgroundIsImage;
	}
	/**
	 * @return the backroundPath
	 */
	public String getBackroundPath() {
		return backroundPath;
	}
	/**
	 * @param backroundPath the backroundPath to set
	 */
	public void setBackroundPath(String backroundPath) {
		this.backroundPath = backroundPath;
	}
	/**
	 * @param conversationID the conversationID to set
	 */
	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}
	/**
	 * @return the messages
	 */
	public ArrayList<ChatMessage> getMessages() {
		return messages;
	}
	/**
	 * @param messages the messages to set
	 */
	public void setMessages(ArrayList<ChatMessage> messages) {
		this.messages = messages;
	}
	/**
	 * @return the conversationName
	 */
	public String getConversationName() {
		return conversationName;
	}
	/**
	 * @param conversationName the conversationName to set
	 */
	public void setConversationName(String conversationName) {
		this.conversationName = conversationName;
	}
	
	public long getLastMessageDate(){
		if (messages.size() == 0){
			return Integer.MAX_VALUE;
		}  
		//return Integer.MAX_VALUE;
		return messages.get(messages.size()-1).getDateInMillis();
		
	}
	/**
	 * @return the iconUrl
	 */
	public String getIconUrl() {
		return iconUrl;
	}
	/**
	 * @param iconUrl the iconUrl to set
	 */
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	/**
	 * @return the postion
	 */
	public int getPostion() {
		return postion;
	}
	/**
	 * @param postion the postion to set
	 */
	public void setPostion(int postion) {
		this.postion = postion;
	}
	
	
	/**
	 * @return the lastvisableMessagesCount
	 */
	public int getLastvisableMessagesCount() {
		return lastvisableMessagesCount;
	}
	/**
	 * @param lastvisableMessagesCount the lastvisableMessagesCount to set
	 */
	public void setLastvisableMessagesCount(int lastvisableMessagesCount) {
		this.lastvisableMessagesCount = lastvisableMessagesCount;
	}
	
	public int getUnReadMessagesCount (){
		if (messages == null || messages.size() == 0){
			return 0;
		}
		return messages.size() - lastvisableMessagesCount;
	}
	public int getLastMessageStatus(){
		if (messages == null || messages.size() == 0){
			return 0;
		}
		return messages.get(messages.size()-1).getStatus();
	}
	
	public String getLatMessageDate(){
		if (messages == null || messages.size() == 0){
			return "";
		}
		
		return DateUtil.formatServerDate(messages.get(messages.size()-1).getCreated());
	}
	
	public String getLastMessages () {
		if (messages == null || messages.size() == 0){
			return "New conversation";
		}
		StringBuilder builder = new StringBuilder();
		
		ChatMessage temp = messages.get(messages.size()-1);
		
		switch (temp.getType()) {
		case ChatMessage.TYPE_TEXT:
		case 11:
			builder.append(temp.getContent());
			break;
		case ChatMessage.TYPE_IMAGE:
		case 13:
			builder.append("Image");
			break;
		case ChatMessage.TYPE_LOCATION:
		case 12:
			builder.append("Location");
			break;
		case ChatMessage.TYPE_AUDIO:
			builder.append("Audio");
			break;
		case ChatMessage.TYPE_VIDEO:
			builder.append("Video");
			break;
		}
		
		String result = builder.toString();
		if (result.length() > 30){
			result = result.substring(0,30) + "...";
		}
		return result;
	}
	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}
	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	/**
	 * @return the color
	 */
	public int getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(int color) {
		this.color = color;
	}
	/**
	 * @return the groupObject
	 */
	public GroupObject getGroupObject() {
		return groupObject;
	}
	/**
	 * @param groupObject the groupObject to set
	 */
	public void setGroupObject(GroupObject groupObject) {
		this.groupObject = groupObject;
	}
	/**
	 * @return the needUpdate
	 */
	public boolean isNeedUpdate() {
		return needUpdate;
	}
	/**
	 * @param needUpdate the needUpdate to set
	 */
	public void setNeedUpdate(boolean needUpdate) {
		this.needUpdate = needUpdate;
	}
	
	public HashMap<String, String> getTempColors (){
		if (this.groupObject != null){
			return groupObject.getTempColors();
		}
		return null;
	}

	
	
}
