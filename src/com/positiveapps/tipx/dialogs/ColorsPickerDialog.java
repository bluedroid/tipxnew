/**
 * 
 */
package com.positiveapps.tipx.dialogs;

import java.util.Locale;


import com.larswerkman.holocolorpicker.ColorPicker;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.util.TextUtil;
import com.positiveapps.tipx.util.ToastUtil;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;



/**
 * @author natiapplications
 *
 */
public class ColorsPickerDialog extends BaseDialogFragment implements OnClickListener{
	
	public static final String DIALOG_NAME = "ColorsPickerDialog";
	
	

	private ColorPicker colorPicker;
	private Button cancelBtn;
	private Button setBtn;
	private int previousColor;
	private DialogCallback callback;
	
	/**
	 * 
	 * @param description
	 * @param credits
	 * @param callback
	 */
	public ColorsPickerDialog(int previousColor,DialogCallback callback){
		//this.mDescription = description;
		this.callback = callback;
		this.previousColor = previousColor;
		
	}
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_color_picker, container, false);
                      
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        
        colorPicker = (ColorPicker)view.findViewById(R.id.picker_color);
        
        cancelBtn = (Button)view.findViewById(R.id.btn_cancel);
        setBtn = (Button)view.findViewById(R.id.btn_set);
        cancelBtn.setOnClickListener(this);
        setBtn.setOnClickListener(this);
        
        if (previousColor == 0){
        	previousColor = colorPicker.getColor();
        }
        colorPicker.setOldCenterColor(previousColor);
       
        return view;
    }


  

    private void dismissDialog(){
    	
    	this.dismiss();
    }
    
    

	@Override
	public void onClick(View v) {
		dismissDialog();
		switch (v.getId()) {
		case R.id.btn_cancel:
			break;
		case R.id.btn_set:
			Integer color = colorPicker.getColor();
			if(callback != null){
	    		callback.onDialogButtonPressed(v.getId(),this,color);
	    	}
			break;
		}
    	
    	
    	
	}



	
	

	
}
