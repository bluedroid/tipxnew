package com.positiveapps.tipx.chat;


import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;

import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.util.DateUtil;
import com.positiveapps.tipx.util.DeviceUtil;



/**
 * Created by gal on 14/12/2014.
 *
 * class SkiMessageGenerator
 *
 * 
 */
public class TipxMessageGenerator {

    private String _attrWrapper = "messageParams";
    private String _messageNS = "http://tipx.positive-apps.com/xmpp";

    public TipxMessageGenerator(){
    }

    public Packet textMessage(String toUserId, String toGroupId,  String text){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "1");
        pe.setValue("tmp_id", DeviceUtil.getDeviceUDID()+DateUtil.getCurrentDateInMilli());
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        pe.setValue("to_user_id", ""+toUserId);
        pe.setValue("to_group_id", ""+toGroupId);
        pe.setValue("lat", ""+TipxApp.generalSettings.getLat());
        pe.setValue("lng", ""+TipxApp.generalSettings.getLon());
        pe.setValue("text", text);
        pe.setValue("date", DateUtil.getCurrentTimeAsServerStringGMT_0());
        int auto_delete = 0;
        if (TipxApp.userSettings.getRemoveMessages()){
        	auto_delete = 1;
        }
        pe.setValue("auto_delete", String.valueOf(auto_delete));
        m.addExtension(pe);
        return m;
    }
  

    public Packet locationMessage(String toUserId, String toGroupId,  String desc){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "2");
        pe.setValue("tmp_id", DeviceUtil.getDeviceUDID()+DateUtil.getCurrentDateInMilli());
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        pe.setValue("to_user_id", ""+toUserId);
        pe.setValue("to_group_id", ""+toGroupId);
        pe.setValue("lat", ""+TipxApp.generalSettings.getLat());
        pe.setValue("lng", ""+TipxApp.generalSettings.getLon());
        pe.setValue("desc", desc);
        pe.setValue("date", DateUtil.getCurrentTimeAsServerStringGMT_0());
        int auto_delete = 0;
        if (TipxApp.userSettings.getRemoveMessages()){
        	auto_delete = 1;
        }
        pe.setValue("auto_delete", String.valueOf(auto_delete));
        m.addExtension(pe);
        return m;
    }

    public Packet mediaMessage(String code,String toUserId, String toGroupId,  String imageData, String desc){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", code);
        pe.setValue("tmp_id", DeviceUtil.getDeviceUDID()+DateUtil.getCurrentDateInMilli());
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        pe.setValue("to_user_id", ""+toUserId);
        pe.setValue("to_group_id", ""+toGroupId);
        pe.setValue("lat", ""+TipxApp.generalSettings.getLat());
        pe.setValue("lng", ""+TipxApp.generalSettings.getLon());
        pe.setValue("media_url", imageData);
        pe.setValue("desc", desc);
        pe.setValue("date", DateUtil.getCurrentTimeAsServerStringGMT_0());
        int auto_delete = 0;
        if (TipxApp.userSettings.getRemoveMessages()){
        	auto_delete = 1;
        }
        pe.setValue("auto_delete", String.valueOf(auto_delete));
        m.addExtension(pe);
        return m;
    }
    
    public Packet acceptMessage(String messageID){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "31");
        pe.setValue("message_id", messageID);
        m.addExtension(pe);
        return m;
    }
    
    public Packet readMessage(String messageID){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "32");
        pe.setValue("message_id", messageID);
        pe.setValue("date",DateUtil.getCurrentTimeAsServerString());
        m.addExtension(pe);
        return m;
    }
    
    public Packet deleteMessage(String messageID){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "13");
        pe.setValue("message_id", messageID);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }

    public Packet confirmDeleteMessage(String messageID){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "33");
        pe.setValue("message_id", messageID);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }
    
    public Packet confirmMediaDownloaded(String messageID){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "15");
        pe.setValue("message_id", messageID);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }

    public Packet confirmDeleteGroup(String groupId){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "34");
        pe.setValue("group_id", groupId);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }
    
    public Packet deleteGroup(String groupId){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "14");
        pe.setValue("group_id", groupId);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }
    
    public Packet deleteConversation(String id){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "37");
        pe.setValue("to_user_id", id);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }
    
    public Packet deleteContact(String id){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "39");
        pe.setValue("to_user_id", id);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }
   

    public Packet startTyping(String userID,String groupID){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "99");
        pe.setValue("to_user_id", userID);
        pe.setValue("to_group_id", groupID);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }
    
    public Packet stopTyping(String userID,String groupID){
        Message m = new Message();
        DefaultPacketExtension pe = new DefaultPacketExtension(this._attrWrapper, this._messageNS);
        pe.setValue("code", "100");
        pe.setValue("to_user_id", userID);
        pe.setValue("to_group_id", groupID);
        pe.setValue("from_id", TipxApp.userProfil.getUserId());
        m.addExtension(pe);
       
        return m;
    }


}
