/**
 * 
 */
package com.positiveapps.tipx.chat;

import com.positiveapps.tipx.objects.ChatMessage;

/**
 * @author natiapplications
 *
 */
public interface TipxChatListener {
	
   public void onChatMessageReceived (ChatMessage msg); 
}
