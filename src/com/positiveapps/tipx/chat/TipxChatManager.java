/**
 * 
 */
package com.positiveapps.tipx.chat;

import java.util.ArrayList;

import org.json.JSONObject;

import android.os.Message;
import android.util.Log;

import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.fragments.ChatFragment;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.util.NotificationUtil;


/**
 * @author natiapplications
 *
 */
public class TipxChatManager {
	
	public static final int ACTION_ACCEPT_IN_USER = 31;
	public static final int ACTION_READED = 32;
	public static final int ACTION_DELETED_MESSAGE = 13;
	public static final int ACTION_DELETED_GROUP = 14;
	public static final int ACTION_CONFIRM_DELETED_MESSAGE = 33;
	public static final int ACTION_CONFIRM_DELETED_GROUP = 34;
	public static final int ACTION_CONFIRM_DELETED_CHAT = 37;
	public static final int ACTION_CONFIRM_DELETED_CONTACT= 39;
	public static final int ACTION_START_TYPING = 99;
	public static final int ACTION_STOP_TYPING= 100;
	public static final int ACTION_CONFIRM_DOWNLOADED= 15;
	/**
	 * 
	 */
	private static TipxChatManager instance;
	private TipxChatManager () {
		
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static TipxChatManager getInstance (){
		if (instance == null){
			instance = new TipxChatManager();
		}
		return instance;
	}
	
	/**
	 * 
	 * @param toUserId
	 * @param toGroupId
	 * @param text
	 */
	public void sendTextMessage (String toUserId, String toGroupId,  String text) {
		TipxMessageGenerator generator = new TipxMessageGenerator();
		TipxApp.smackManager.sendMessage(generator.textMessage(toUserId, toGroupId, text));
	}
	
	/**
	 * 
	 * @param toUserId
	 * @param toGroupId
	 * @param imageData
	 * @param desc
	 */
	public void sendMediaMessage (String code,String toUserId, String toGroupId,  String imageData, String desc){
		TipxMessageGenerator generator = new TipxMessageGenerator();
		TipxApp.smackManager.sendMessage(generator.mediaMessage(code,toUserId, toGroupId, imageData, desc));
	}
	
	/**
	 * 
	 * @param toUserId
	 * @param toGroupId
	 * @param desc
	 */
	public void sendLocationMessage (String toUserId, String toGroupId,  String desc){
		TipxMessageGenerator generator = new TipxMessageGenerator();
		TipxApp.smackManager.sendMessage(generator.locationMessage(toUserId, toGroupId, desc));
	}
	
	
	/**
	 * 
	 * @param conveId
	 * @param messageId
	 */
	public void removeMessage (String conveId,String messageId) {
		TipxApp.conversationManager.removeMessageByID(conveId, messageId);
		//sendStatusMessage(ACTION_DELETED_MESSAGE, messageId);
		TipxApp.networkManager.sendDeleteMessage(messageId);
	}
	
	
	/**
	 * 
	 * @param conveId
	 * @param groupID
	 */
	public void removeGroup (String conveId,String groupID) {
		TipxApp.conversationManager.removeConversation(groupID);
		sendStatusMessage(ACTION_DELETED_GROUP, groupID,"",true);
	}
	
	
	/**
	 * 
	 * @param type
	 * @param id
	 */
	public void sendStatusMessage (int type,String id,String senderID,boolean sendByAPI) {
		Log.i("messageStatuslog", "action - " + type);
		TipxMessageGenerator generator = new TipxMessageGenerator();
		switch (type) {
		case ACTION_ACCEPT_IN_USER:
			TipxApp.smackManager.sendMessage(generator.acceptMessage(id));
			if (sendByAPI){
				sendMessageStatusUsingAPI(type, id, senderID);
			}
			break;
		case ACTION_READED:
			TipxApp.smackManager.sendMessage(generator.readMessage(id));
			sendMessageStatusUsingAPI(type, id, senderID);
			break;
		case ACTION_CONFIRM_DELETED_MESSAGE:
			TipxApp.smackManager.sendMessage(generator.confirmDeleteMessage(id));
			sendMessageStatusUsingAPI(type, id, senderID);
			break;
		case ACTION_CONFIRM_DELETED_GROUP:
			TipxApp.smackManager.sendMessage(generator.confirmDeleteGroup(id));
			break;
		case ACTION_DELETED_MESSAGE:
			TipxApp.smackManager.sendMessage(generator.deleteMessage(id));
			break;
		case ACTION_DELETED_GROUP:
			TipxApp.smackManager.sendMessage(generator.deleteGroup(id));
			break;
		case ACTION_CONFIRM_DELETED_CHAT:
			TipxApp.smackManager.sendMessage(generator.deleteConversation(id));
			break;
		case ACTION_CONFIRM_DELETED_CONTACT:
			TipxApp.smackManager.sendMessage(generator.deleteContact(id));
			break;
		case ACTION_CONFIRM_DOWNLOADED:
			TipxApp.smackManager.sendMessage(generator.confirmMediaDownloaded(id));
			if (sendByAPI){
				TipxApp.networkManager.confirmMediaDownloaded(id, null);
			}
			break;
		
		}
		
	}
	
	public void sendMessageStatusUsingAPI (final int type,final String id,final String senderId){
		
		if (com.positiveapps.tipx.MainActivity.activityISsOn){
			if (TipxApp.smackManager != null && TipxApp.smackManager.isConnected()){
				return;
			}
		}
		Log.e("sendMessageStatusAPILog", "sendMessageStatusByAPI");
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(2000);
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("ID", id);
						//jsonObject.put("SenderID", senderId);
						jsonObject.put("Code", type);
						StringBuilder stringBuilder = new StringBuilder();
						stringBuilder.append("[");
						stringBuilder.append(jsonObject.toString());
						stringBuilder.append("]");
						Log.e("sendMessageStatusAPILog", "messages: " + stringBuilder.toString());
						TipxApp.networkManager.updateMessageStatus(stringBuilder.toString(), null);
					} catch (Exception e) {
						Log.e("sendMessageStatusAPILog", "som ex " + e.getMessage() );
						e.printStackTrace();
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}).start();
		
		
		
     }
	
	
	public void sendStatusMessage (int type,String id,boolean isGroup) {
		Log.i("messageStatuslog", "action - " + type);
		TipxMessageGenerator generator = new TipxMessageGenerator();
		
		switch (type) {
		case ACTION_START_TYPING:
			if (isGroup){
				TipxApp.smackManager.sendMessage(generator.startTyping("0", id));
			}else{
				TipxApp.smackManager.sendMessage(generator.startTyping(id, "0"));
			}
			break;
		case ACTION_STOP_TYPING:
			if (isGroup){
				TipxApp.smackManager.sendMessage(generator.stopTyping("0", id));
			}else{
				TipxApp.smackManager.sendMessage(generator.stopTyping(id, "0"));
			}
			break;
		}
		
	}
	
	
	
	/**
	 * 
	 * @param msg
	 */
	public boolean handleReceivedMessage (ChatMessage msg){
		Log.e("chatmanagerlog","handleMessage. messageType = " + msg.getType());
		boolean result = true;
		switch (msg.getType()) {
		case ChatMessage.TYPE_TEXT:
		case ChatMessage.TYPE_LOCATION:
		case ChatMessage.TYPE_IMAGE:
		case ChatMessage.TYPE_ME_JOIN:
		case ChatMessage.TYPE_ME_LEAVE:
		case ChatMessage.TYPE_MEMBER_JOIN:
		case ChatMessage.TYPE_MEMBER_LEAVE:
		case 11:
		case 12:
		case 13:
		case ChatMessage.TYPE_VIDEO:
		case ChatMessage.TYPE_AUDIO:
			result = handelNewMessage(msg);
			break;
		case ChatMessage.TYPE_MESSAGE_ACCEPT_IN_SERVER:
		case ChatMessage.TYPE_MESSAGE_ACCEPT_IN_USER:
		case ChatMessage.TYPE_MESSAGE_READED:
		case ChatMessage.TYPE_CONFIRM_DELETED:
			handelUpdateMessage(msg);
			break;
		case ChatMessage.TYPE_MESSAGE_DELETED:
			handelRemoveMessage(msg);
			break;
		case ChatMessage.TYPE_GROUP_DELETED:
			handelRemoveGroup(msg);
			break;
		case ChatMessage.TYPE_DELET_CONVERSATION:
			handelRemoveConversation(msg);
			break;
		case ChatMessage.TYPE_DELET_CONTACT:
			handelRemoveContact(msg);
			break;
		
		}
		return result;
	}

	/**
	 * @param msg
	 */
	private void handelRemoveContact(ChatMessage msg) {
		TipxApp.contactsManager.removeContactByContactId(msg.getSenderId());
		TipxApp.conversationManager.removeConversation(msg.getSenderId());
		sendStatusMessage(ACTION_CONFIRM_DELETED_CONTACT, msg.getSenderId(),"",true);
		//NotificationUtil.updateNotification();
	}


	/**
	 * @param msg
	 */
	private void handelRemoveConversation(ChatMessage msg) {
		TipxApp.conversationManager.removeConversation(msg.getSenderId());
		sendStatusMessage(ACTION_CONFIRM_DELETED_CHAT, msg.getSenderId(),"",true);
		//NotificationUtil.updateNotification();
	}


	/**
	 * @param msg
	 */
	private void handelRemoveGroup(ChatMessage msg) {
		TipxApp.conversationManager.removeConversation(msg.getGroupId()+"");
		sendStatusMessage(ACTION_CONFIRM_DELETED_GROUP, msg.getGroupId()+"","",true);
		//NotificationUtil.updateNotification();
	}

	/**
	 * @param msg
	 */
	public void handelRemoveMessage(ChatMessage msg) {
		Log.e("confirmDeleteLog", "delete message !") ;
		TipxApp.conversationManager.removeMessageByID(msg.getMessageId()+"");
		sendStatusMessage(ACTION_CONFIRM_DELETED_MESSAGE, msg.getMessageId()+"",msg.getSenderId(),true);
		TipxApp.networkManager.confirmDeletation(msg.getMessageId()+"",msg.getConversationId());
		if (ChatFragment.chatHandeler != null){
			Message message = new Message();
			message.what = 11;
			message.obj = msg;
			ChatFragment.chatHandeler.dispatchMessage(message);
		}
		//NotificationUtil.updateNotification();
	}

	/**
	 * @param msg
	 */
	private void handelUpdateMessage(ChatMessage msg) {
		Log.e("handleUpdateMessagLog", "handelUpdateMessage " + msg.getType());
		switch (msg.getType()) {
		case ChatMessage.TYPE_MESSAGE_ACCEPT_IN_SERVER:
			msg.setStatus(ChatMessage.STATUS_RECIVED_IN_SERVER);
			TipxApp.conversationManager.updateMessageStatusByTempId(msg.getTempId(), msg);
			break;
		case ChatMessage.TYPE_MESSAGE_ACCEPT_IN_USER:
			msg.setStatus(ChatMessage.STATUS_RECIVED_IN_RECIPIENT);
			boolean sccess = TipxApp.conversationManager.updateMessageStatusById(msg.getMessageId()+"", msg);
			Log.e("sccessLog", "sccess ? " + sccess);
			if (!sccess){
			//	TipxApp.conversationManager.updateMessageStatusByTempId(msg.getTempId(), msg);
			}
			break;
		case ChatMessage.TYPE_MESSAGE_READED:
			msg.setStatus(ChatMessage.STATUS_READED_BY_RECIPIENT);
			TipxApp.conversationManager.updateMessageStatusById(msg.getMessageId()+"", msg);
			break;
		case ChatMessage.TYPE_CONFIRM_DELETED:
			msg.setStatus(ChatMessage.STATUS_CONFIRM_DELETATION);
			Log.e("confirmlog", "type - " + msg.getStatus() + " id = " + msg.getMessageId());
			TipxApp.conversationManager.updateMessageStatusById(msg.getMessageId()+"", msg);
			break;
		}
		
	}

	/**
	 * @param msg
	 */
	private boolean handelNewMessage(ChatMessage msg) {
		Log.d("chatmanagerlog","handleNewMessage. messageChatType = " + msg.getChatType() + " id " + msg.getSenderId()
				+ " groupId " + msg.getGroupId() 
				+ " reciid = " + msg.getRecipientId());
		
		msg = setMessageReadStatusBeforSaving(msg);
		boolean result = true;
		if (msg.getChatType() == ChatMessage.GROUP_MESSAGE){
			if (msg.getType() == ChatMessage.TYPE_ME_LEAVE){
				TipxApp.conversationManager.removeConversation(msg.getGroupId()+"");
			}else if(msg.getType() == ChatMessage.TYPE_MEMBER_LEAVE){
				TipxApp.conversationManager.removeMessagesFromConversaionByRemovedId(msg.getGroupId()+"", msg.getSenderId());
			}else {
				TipxApp.conversationManager.addMessageByConversationID(msg.getGroupId()+"", msg);
			}
		}else{
			Log.d("chatmanagerlog","handleNewMessage. userId = " + msg.getSenderId()+" || "+msg.getRecipientId());
			if (msg.getMessagePhat() == ChatMessage.PHAT_TO){
				TipxApp.conversationManager.addMessageByConversationID(msg.getRecipientId()+"", msg);
			}else{
				String senderId = TipxApp.contactsManager.getContactIdByContactPhone(msg.getSenderPhone());
				msg.setSenderId(senderId);
				ArrayList<ChatMessage> res = 
						TipxApp.conversationManager.addMessageByConversationID(msg.getSenderId()+"", msg);
				if (res == null){
					result = false;
				}
			}
			
		}
		if (msg.getMessagePhat() == ChatMessage.PHAT_FROM){
			if (TipxApp.smackManager != null){
				if (TipxApp.smackManager.isConnected()){
					sendStatusMessage(ACTION_ACCEPT_IN_USER, msg.getMessageId()+"",msg.getSenderId(),true);
				}else{
					sendMessageStatusUsingAPI(ACTION_ACCEPT_IN_USER, msg.getMessageId()+"",msg.getSenderId());
				}
			}else{
				sendMessageStatusUsingAPI(ACTION_ACCEPT_IN_USER, msg.getMessageId()+"",msg.getSenderId());
			}
			
		}
		       
		return result;
	}
	
	
	private ChatMessage setMessageReadStatusBeforSaving (ChatMessage msg){
		if (msg.getMessagePhat() == ChatMessage.PHAT_FROM){
			if (!TipxApp.userSettings.getSendReadConfirmation()){
				msg.setReadConfirmationStatus(ChatMessage.READ_STATUS_CANCEL);
			}else{
				msg.setReadConfirmationStatus(ChatMessage.READ_STATUS_WAIT);
			}
		}
		
		return msg;
	}
	
	public void notifyUserNewChatMessage(ChatMessage chatMessage){
		
		Log.e("notifichatlog", "notifyUserNewChatMessage. type - " + chatMessage.getType());
		switch (chatMessage.getType()) {
		case 11:
		case 12:
		case 13:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			String id = "";
			if (chatMessage.getChatType() == ChatMessage.GROUP_MESSAGE){
				id = chatMessage.getGroupId()+"";
			}else{
				if (chatMessage.getMessagePhat() == ChatMessage.PHAT_TO){
					id = chatMessage.getRecipientId()+"";
				}else{
					id = TipxApp.contactsManager.getContactIdByContactPhone(chatMessage.getSenderPhone());
				}
			}
			NotificationUtil.sendChatNotification(id);
			break;
		}
	}

}
