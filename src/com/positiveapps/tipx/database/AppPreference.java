/**
 * 
 */
package com.positiveapps.tipx.database;



import com.positiveapps.tipx.TipxApp;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * @author Nati Gabay
 *
 */
public class AppPreference {

	
	public static final int UNITS_TYPE_KM = 1;
	public static final int UNITS_TYPE_MILES = 2;
	public static final int SKI_PASS_TYPE_AREA = 1;
	public static final int SKI_PASS_TYPE_RESORT = 2;
	
	
	private static AppPreference instance;
	private static Context context = TipxApp.appContext;
	
	private static PreferenceUserProfil userProfil;
	private static PreferenceUserSettings userSettings;
	private static PreferenceGeneralSettings generalSettings;
	
	private AppPreference (Context context){
		userSettings = PreferenceUserSettings.getUserSettingsInstance();
		userProfil = PreferenceUserProfil.getUserProfileInstance();
		generalSettings = PreferenceGeneralSettings.getGeneralSettingsInstance();
	}
	
	public static AppPreference getInstans(Context context){
		if(instance == null){
			instance = new AppPreference(context);
		}
		setInstancContext(context);
		return instance;
	}
	
	public static void removeInstance(){
		userSettings.removeInstance();
		userProfil.removeInstance();
		generalSettings.removeInstance();
		instance = null;
	}
	
	private static void setInstancContext (Context context){
		AppPreference.context = context;
	}
	
	
	
	/**
	 * @return the userProfil
	 */
	public PreferenceUserProfil getUserProfil() {
		return userProfil;
	}

	/**
	 * @param userProfil the userProfil to set
	 */
	public void setUserProfil(PreferenceUserProfil userProfil) {
		this.userProfil = userProfil;
	}

	/**
	 * @return the userSettings
	 */
	public PreferenceUserSettings getUserSettings() {
		return userSettings;
	}

	/**
	 * @param userSettings the userSettings to set
	 */
	public void setUserSettings(PreferenceUserSettings userSettings) {
		this.userSettings = userSettings;
	}
	
	/**
	 * @return the generalSettings
	 */
	public PreferenceGeneralSettings getGeneralSettings() {
		return generalSettings;
	}

	/**
	 * @param generalSettings the generalSettings to set
	 */
	public void setGeneralSettings(PreferenceUserSettings generalSettings) {
		this.userSettings = generalSettings;
	}


	public static class PreferenceGeneralSettings {

		public final String GENERAL_SETTINGS = "GeneralSettings";

		public static final String LAT = "Lat";
		public static final String LON = "Lon";
		public static final String GCM_KEY = TipxApp.appVersion + "GcmKey";
		
		public static final String EMERGENCY_PHONE_1 = "EmergencyPhone1";
		public static final String EMERGENCY_PHONE_2 = "EmergencyPhone2";
		public static final String SCREEN_WIDTH = "ScreenWidth";
		public static final String SCREEN_HIGHT = "ScreenHight";
		public static final String LAST_READ_MESSAGE_COUNT = "LastReadMessageCount";
		

		public static final String SERVER_IP = "Server_ip";
		public static final String SERVER_REQUEST_PORT = "Server_request_port";
		public static final String SERVER_XMPP_PORT = "Server_xmpp_port";
		public static final String SERVER_APP_VERTION = "Server_app_vertion";
		public static final String SERVER_NEW_VERTION = "Server_new_vertion";
		public static final String LAST_MESSAGE = "Last_message";
		 
		public static final String CONTACT_SCREEN_EXPLANATION = "ContactScreenExplanation";
		public static final String CHATS_SCREEN_EXPLANATION = "ChatScreenExplanation";
		
		private SharedPreferences generalSettings;
		public static PreferenceGeneralSettings generalSettingInstance;

		private PreferenceGeneralSettings() {
			generalSettings = context.getSharedPreferences(GENERAL_SETTINGS,
					Context.MODE_PRIVATE);
		}

		public static PreferenceGeneralSettings getGeneralSettingsInstance() {
			if (generalSettingInstance == null) {
				generalSettingInstance = new PreferenceGeneralSettings();
			}
			return generalSettingInstance;
		}
		
		public void removeInstance(){
			generalSettings.edit().clear().commit();
			generalSettingInstance = null;
		}

		
		public void setServerIP(String toSet) {
			generalSettings.edit().putString(SERVER_IP, toSet).commit();
		}
		
		
		public void setServerRequestPort(String toSet) {
			generalSettings.edit().putString(SERVER_REQUEST_PORT, toSet).commit();
		}
		
		
		public void setServerXmppPort(int toSet) {
			generalSettings.edit().putInt(SERVER_XMPP_PORT, toSet).commit();
		}
		
		
		public void setServerAppVerstion(String toSet) {
			generalSettings.edit().putString(SERVER_APP_VERTION, toSet).commit();
		}
		
		public void setServerNewVerstion(String toSet) {
			generalSettings.edit().putString(SERVER_NEW_VERTION, toSet).commit();
		}
		
		public void setLat(String toSet) {
			generalSettings.edit().putString(LAT, toSet).commit();
		}

		public void setLon(String toSet) {
			generalSettings.edit().putString(LON, toSet).commit();
		}
		
		public void setGCMKey(String gcmKey) {
			generalSettings.edit().putString(GCM_KEY, gcmKey).commit();
		}
		
		public void setEmergencyPhone1(String phone) {
			generalSettings.edit().putString(EMERGENCY_PHONE_1, phone).commit();
		}
		
		public void setEmergencyPhone2(String phone) {
			generalSettings.edit().putString(EMERGENCY_PHONE_2, phone).commit();
		}
		
		public void setScreenWidth(int width) {
			generalSettings.edit().putInt(SCREEN_WIDTH, width).commit();
		}
		
		public void setScreenHight(int hight) {
			generalSettings.edit().putInt(SCREEN_HIGHT, hight).commit();
		}

		public void setLastReadMessageCount(String groupID,int count) {
			generalSettings.edit().putInt(groupID + LAST_READ_MESSAGE_COUNT, count).commit();
		}
		
		public void setLastText (String set,String conversionID){
			generalSettings.edit().putString(conversionID + LAST_MESSAGE,set).commit();
		}
		
		public String getLastText(String converstionID){
			return generalSettings.getString(converstionID + LAST_MESSAGE,"");
		}
		public void setContactScreenExplanationShow(boolean set) {
			generalSettings.edit().putBoolean(CONTACT_SCREEN_EXPLANATION, set).commit();
		}
		
		public void setChatScreenExplanationShow(boolean set) {
			generalSettings.edit().putBoolean(CHATS_SCREEN_EXPLANATION, set).commit();
		}
		
		public String getLat() {
			return generalSettings.getString(LAT, "0.0");
		}

		public String getLon() {
			return generalSettings.getString(LON, "0.0");
		}
		
		
		public String getLocation () {
			return getLat()+","+getLon();
		}
		
		public String getGCMKey () {
			return generalSettings.getString(GCM_KEY, "");
		}
		
		public String getEmergencyPhone1 () {
			return generalSettings.getString(EMERGENCY_PHONE_1, "");
		}
		
		public String getEmergencyPhone2 () {
			return generalSettings.getString(EMERGENCY_PHONE_2, "");
		}
		
		public int getScreenWidth () {
			return generalSettings.getInt(SCREEN_WIDTH, 0);
		}
		
		public int getScreenHight () {
			return generalSettings.getInt(SCREEN_HIGHT, 0);
		}
		
		public int getLastReadMessageCount (String groupId) {
			return generalSettings.getInt(groupId + LAST_READ_MESSAGE_COUNT, 0);
		}
		
		public String getServerIP() {
			return generalSettings.getString(SERVER_IP, "54.72.134.0");
		}
		
		public String getServerRequestPort() {
			return generalSettings.getString(SERVER_REQUEST_PORT, "8085");
		}
		
		public int getServerXmppPort() {
			return generalSettings.getInt(SERVER_XMPP_PORT, 5224);
		}
		
		public String getServerAppVerstion() {
			return generalSettings.getString(SERVER_APP_VERTION, "1.0");
		}
		
		public String getServerNewVerstion() {
			return generalSettings.getString(SERVER_NEW_VERTION, getServerAppVerstion());
		}
		
		
		public boolean isContactScreenExplanationShown() {
			return generalSettings.getBoolean(CONTACT_SCREEN_EXPLANATION, false);
		}
		
		public boolean isCahtsSCreenEXplanationShown() {
			return generalSettings.getBoolean(CHATS_SCREEN_EXPLANATION, false);
		}

	}



	public static class PreferenceUserProfil {
		
		public static final String USER_PROFILE = "UserProfile";
		
		public static final  String USER_ID = "user_id";
		
		
		
		private static final String NAME = "Name";
		private static final String IMAGE_PATH = "ImagePath";
		private static final String STATUS = "Status";
		
		private static final String EMAIL = "Email";
		private static final String USER_PHONE = "UserPhone";
	    private static final String NOTIFICATION_COUNTER = "NotificationCounter";
	    private static final String LAST_INBOX_COUTNT = "LastInboxCount";
	    
	    private static final String PASS_CODE = "PassCode";
		private static final String PASS = "Password";
		private static final String SECRET = "Secret";
		private static final String ICON_URL = "IconUrl";
		private static final String COUNTRY_NAME = "CountryName";
		private static final String COUNTRY_CODE = "CountryCode";
		private static final String CITY = "City";
		private static final String LAST_LAT = "LastLat";
		private static final String LAST_LON = "LastLng";
		private static final String MY_PHUS_SERVER_ID = "MyPushServerID";
		private static final String FBID = "FBID";
		private static final String CREATED_AT = "Created";
		private static final String UPDATE_INTERVAL = "ApdateInterval";
		
		private SharedPreferences userProfile;
		private static PreferenceUserProfil userProfileInstance;
		
		private  PreferenceUserProfil (){
			userProfile = context.getSharedPreferences(USER_PROFILE, Context.MODE_PRIVATE);
		}
		
		public  static PreferenceUserProfil getUserProfileInstance (){
			if (userProfileInstance == null){
				userProfileInstance = new PreferenceUserProfil();
			}
			return userProfileInstance;
		}
		
		public  void removeInstance(){
			userProfile.edit().clear().commit();
			userProfileInstance = null;
		}
		
		public void setUserId(String userId) {
			userProfile.edit().putString(USER_ID, userId).commit();
		}

		public void setUserNaem(String name) {
			userProfile.edit().putString(NAME, name).commit();
		}
		
		public void setUserImagePath(String path) {
			userProfile.edit().putString(IMAGE_PATH, path).commit();
		}
		
		public void setUserStatus(String status) {
			userProfile.edit().putString(STATUS, status).commit();
		}

		public void setUserEmail(String email) {
			userProfile.edit().putString(EMAIL, email).commit();
		}

		
		public void setNotificationCount(int credits) {
			userProfile.edit().putInt(NOTIFICATION_COUNTER, credits).commit();
		}
		
		public void setLastInboxCoutn(int credits) {
			userProfile.edit().putInt(LAST_INBOX_COUTNT, credits).commit();
		}
		
		public void setUserPhone(String phone) {
			userProfile.edit().putString(USER_PHONE, phone).commit();
		}
		
		public void setUserPassCode(String passCode) {
			userProfile.edit().putString(PASS_CODE, passCode).commit();
		}
		public void setUserPass(String pass) {
			userProfile.edit().putString(PASS, pass).commit();
		}
		public void setUserCountry(String country) {
			userProfile.edit().putString(COUNTRY_NAME, country).commit();
		}
		public void setUserCountryCode(String code) {
			userProfile.edit().putString(COUNTRY_CODE, code).commit();
		}
		public void setUserCity(String city) {
			userProfile.edit().putString(CITY, city).commit();
		}
		public void setUserFBID(String fbid) {
			userProfile.edit().putString(FBID, fbid).commit();
		}
		public void setUserSecret(String secret) {
			userProfile.edit().putString(SECRET, secret).commit();
		}
		public void setUserLastLat(String lastlat) {
			userProfile.edit().putString(LAST_LAT, lastlat).commit();
		}
		public void setUserLastLon(String lastlon) {
			userProfile.edit().putString(LAST_LON, lastlon).commit();
		}
		public void setUserCreatedAt(String createdAt) {
			userProfile.edit().putString(CREATED_AT, createdAt).commit();
		}
		public void setUserPushId(String pushId) {
			userProfile.edit().putString(MY_PHUS_SERVER_ID, pushId).commit();
		}
		public void setUserIconUrl(String iconUrl) {
			userProfile.edit().putString(ICON_URL, iconUrl).commit();
		}
		public void setUserApdateInterval(int time) {
			userProfile.edit().putInt(UPDATE_INTERVAL, time).commit();
		}
		
		
		
		public String getUserId() {
			return userProfile.getString(USER_ID, "0");
		}

		public String getUserName() {
			return userProfile.getString(NAME, "");
		}
		
		public String getUserImagePath() {
			return userProfile.getString(IMAGE_PATH, "");
		}
		
		public String getUserStatus() {
			return userProfile.getString(STATUS, "");
		}

		public String getUserEmail() {
			return userProfile.getString(EMAIL, "");
		}

		public int getNotificationCounter() {
			return userProfile.getInt(NOTIFICATION_COUNTER,0);
		}
		
		public int getLastInboxCount() {
			return userProfile.getInt(LAST_INBOX_COUTNT,0);
		}
		
		public String getUserPhone() {
			return userProfile.getString(USER_PHONE, "");
		}
		
		public String getUserPassCode() {
			return userProfile.getString(PASS_CODE, "");
		}
		public String getUserPass() {
			return userProfile.getString(PASS, "");
		}
		public String getUserSecret() {
			return userProfile.getString(SECRET, "");
		}
		public String getUserCountry() {
			return userProfile.getString(COUNTRY_NAME, "");
		}
		public String getUserCountryCode() {
			return userProfile.getString(COUNTRY_CODE, "");
		}
		public String getUserCity() {
			return userProfile.getString(CITY, "");
		}
		public String getUserLastLat() {
			return userProfile.getString(LAST_LAT, "");
		}
		public String getUserLastLon() {
			return userProfile.getString(LAST_LON, "");
		}
		public String getUserFBID() {
			return userProfile.getString(FBID, "");
		}
		public String getUserCreatedAt() {
			return userProfile.getString(CREATED_AT, "");
		}
		public String getUserIconUrl() {
			return userProfile.getString(ICON_URL, "");
		}
		public String getUserPushID() {
			return userProfile.getString(MY_PHUS_SERVER_ID, "");
		}
		public int getUserUpdateIntervall() {
			return userProfile.getInt(UPDATE_INTERVAL, 30);
		}
		
		
		


	}
	
	public static class PreferenceUserSettings {
		
		public  final String USER_SETTINGS = "UserSettings";
		
		private static final String DISPLAY_LAST_LOGIN_TIME = "DisplayLastLoginTime";
		private static final String SHOW_PROFILE_PICTURE = "ShowProfilePicture";
		private static final String PRESENT_STATUS = "PresentStatus";
		private static final String REMOVE_MESSAGES = "RemoveMessages";
		private static final String LOCK_PASSWORD = "LockPassword";
		private static final String LOCK_PASSWORD_NUMBER = "LockPasswordNumber";
		private static final String SEND_READ_CONFIRMATION = "SendReadConvfirmation";

		private SharedPreferences userSettings;
		public static  PreferenceUserSettings userSettingInstance;
		
		private PreferenceUserSettings() {
			userSettings = context.getSharedPreferences(USER_SETTINGS, Context.MODE_PRIVATE);
		}
		
		public static PreferenceUserSettings getUserSettingsInstance() {
			if (userSettingInstance == null){
				userSettingInstance = new PreferenceUserSettings();
			}
			return userSettingInstance;
		}
		
		public void removeInstance(){
			userSettings.edit().clear().commit();
			userSettingInstance = null;
		}
		
		
		// SET
		
		
		public void setDisplayLastLoginTime(boolean set) {
			userSettings.edit().putBoolean(DISPLAY_LAST_LOGIN_TIME, set).commit();
		}
		public void setShowProfilePicture(boolean set) {
			userSettings.edit().putBoolean(SHOW_PROFILE_PICTURE, set).commit();
		}
		public void setPresentStatus(boolean set) {
			userSettings.edit().putBoolean(PRESENT_STATUS, set).commit();
		}
		public void setRemoveMessages(boolean set) {
			userSettings.edit().putBoolean(REMOVE_MESSAGES, set).commit();
		}
		public void setLockPassword(boolean set) {
			userSettings.edit().putBoolean(LOCK_PASSWORD, set).commit();
		}
		public void setSendReadConfirmation(boolean set) {
			userSettings.edit().putBoolean(SEND_READ_CONFIRMATION, set).commit();
		}
		public void setLockPasswordNumber(String pass) {
			userSettings.edit().putString(LOCK_PASSWORD_NUMBER, pass).commit();
		}
		
		
		// GET
		
		public boolean getDisplayLastLoginTime() {
			return userSettings.getBoolean(DISPLAY_LAST_LOGIN_TIME, true);
		}
		public boolean getShowProfilPicture() {
			return userSettings.getBoolean(SHOW_PROFILE_PICTURE, true);
		}
		public boolean getPresentStatus() {
			return userSettings.getBoolean(PRESENT_STATUS, true);
		}
		public boolean getRemoveMessages() {
			return userSettings.getBoolean(REMOVE_MESSAGES, false);
		}
		public boolean getLockPassword() {
			return userSettings.getBoolean(LOCK_PASSWORD, false);
		}
		public boolean getSendReadConfirmation() {
			return userSettings.getBoolean(SEND_READ_CONFIRMATION, true);
		}
		public String getLockPasswordNumber() {
			return userSettings.getString(LOCK_PASSWORD_NUMBER, "");
		}
			
		

	}
	
	
	
	
	
	
	
	

}
