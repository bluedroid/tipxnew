package com.positiveapps.tipx;




import com.positiveapps.tipx.fragments.login.RegistrationFragment;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.FragmentsUtil;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;


public class LoginActivity extends FragmentActivity implements OnClickListener{


	
	private boolean activityIsOn;
	public static Fragment currentFragment;
	
	public static TextView screenTitle;
	public static LinearLayout buttonsContainer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeNoTitleActivity(this);
		setContentView(R.layout.activity_login);
		
		screenTitle = (TextView)findViewById(R.id.screen_title);
		buttonsContainer = (LinearLayout)findViewById(R.id.action_bar_buttons_container);
		
		activityIsOn = true;
		FragmentsUtil.addFragment(getSupportFragmentManager(),
				RegistrationFragment.newInstance(), R.id.login_container);
	}
    
	public static void openNewLoginFragment (FragmentManager fm,Fragment toOpen){
		FragmentsUtil.openFragmentRghitToLeft(fm, toOpen, R.id.login_container);
	}

	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		activityIsOn = false;
	}
	
	
	public static void setScreenTitle (String text){
		if (text != null){
			screenTitle.setText(text);
		}else{
			screenTitle.setText(TipxApp.appContext.getString(R.string.app_title));
		}
	}
	
	public static void addButtonToActionBar (ActionBarButton barButton){
		buttonsContainer.addView(barButton.getView());
	}
	
	public static void removeAllActionBarButtons(){
		buttonsContainer.removeAllViews(); 
	}
	
	public static void removeButtonFromActionBar (View toRemove){
		buttonsContainer.removeView(toRemove);
	}
	
	@Override
	public void onClick(View v) { 
		
		switch (v.getId()) {
		case R.id.choose_country_item:
		
			break;
		case R.id.send_btn:
			
			
			break;
		}
		
	}
	

	
}
